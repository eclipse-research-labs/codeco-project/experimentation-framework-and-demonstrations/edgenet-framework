# CODECO Experimentation Framework (CODEF)

CODEF requires only **Docker** for execution, while it supports additional cloud-edge capabilities and environments providing experimentation automation features through its innovative abstraction layers.

Besides the supported native or 3rd party applications, the CODEF currently supports the installation of CODECO solutions like the [ACM component](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/tree/v2/edgenet-framework?ref_type=heads), [L2S-M solution of NetMA](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/network-management-and-adaptation-netma), as well as EdgeNet. 

Details about the CODEF can be found in the [codeco-experimentation-framework folder](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/edgenet-framework/-/tree/v2/codeco-experimentation-framework?ref_type=heads), while the EdgeNet support is located in the [edgenet-framework folder](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/tree/v2/edgenet-framework?ref_type=heads).

**Please note that due to EdgeNet status, its installation and related code is currently not working**.
