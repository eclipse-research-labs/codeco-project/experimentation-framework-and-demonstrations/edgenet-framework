# CODECO Experimentation Framework
The **CODECO Experimentation Framework** (also referred to as CODEF) is an open-source solution for the rapid experimentation of K8s-based edge cloud deployments. It adopts a microservice-based architecture and introduces innovative abstractions for: 
1. **holistic** deployment of Kubernetes clusters and associated applications, starting from the VM allocation level,
2. **declarative** cross-layer experiment configuration,
3. **automation** features that execute the experiment configuration up to the visualization of produced results. 

## Architecture
It extends the [Clusterslice](https://github.com/SWNRG/clusterslice/tree/main) solution, following a more lightweight approach, as it requires only docker for execution, and supporting additional edge capabilities and environments (e.g., EdgeNet) and experimentation automation features through its innovative abstractions. The framework's architecture includes the following components:
+ **Experiment Manager**: the "boss" which oversees and coordinates all experiment processes, ensuring the reliable execution, reproducibility, automatic deployment and control of experiments.
+ **Infrastructure Manager**: responsible for allocating the cluster nodes over heterogeneous test-beds and cloud systems (i.e. including physical nodes or VMs) and the OS installation.
+ **Resource Managers**: provide a node-level automation abstraction for the software deployment of cluster nodes. They oversee the deployment of user-defined applications through the utilization of [Ansible playbook templates](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/tree/main/codeco-experimentation-framework/containers/resource-manager/playbooks?ref_type=heads).
+ **Experiment Controller**: responsible for the simple and automated experimentation execution and result creation. The user just defines the experiments, the specific metrics and the number of replications.
+ **Results Processor**: evaluates and post-processes the experiment results. Performs statistical evaluations (e.g. mean values), plotting based on the pre-defined metrics, and automating the creation of LaTeX-based report PDF files, incorporating the generated plots.

![CODEF Architecture](docs/CODEF_architecture.png)

**Note: For more details please refer to the documentation in the [docs/](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/tree/main/codeco-experimentation-framework/docs?ref_type=heads) folder.**


## List of directories and scripts

| Directory / Script Name       | Description                                                | 
|-------------------|------------------------------------------------------------|
| [`basic_functions.sh`](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/blob/main/codeco-experimentation-framework/basic_functions.sh?ref_type=heads)      |  Script used for troubleshooting and function parsing  | 
| [`cluster_deployment.sh`](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/blob/main/codeco-experimentation-framework/cluster_deployment.sh) |   Script used for CODEF functionality, Infrastructure Manager deployment and docker-compose.yaml generation. **Note to Enable/Disable DHCP if needed**   |
| [`create_metrics.sh`](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/blob/main/codeco-experimentation-framework/create_metrics.sh?ref_type=heads) |   Script used for the metrics creation and LateX generation     |
| [`experiments_controller.sh`](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/blob/main/codeco-experimentation-framework/experiments_controller.sh?ref_type=heads) |   Script used for the automated experiments using KNB. Used by the scripts in the [examples](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/blob/main/codeco-experimentation-framework/examples?ref_type=heads) folder  |
| [`get_results_net.sh`](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/blob/main/codeco-experimentation-framework/get_results_net.sh?ref_type=heads) |   Script used to calculate and display average values of the KNB experiment   |
| [`import_configuration.sh`](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/blob/main/codeco-experimentation-framework/import_configuration.sh?ref_type=heads) |   Script used for troubleshooting i.e. if a specific configuration file exists and CODEF installation to different infrastructures  |
| [`ssh_without_password.sh`](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/blob/main/codeco-experimentation-framework/ssh_without_password.sh?ref_type=heads) |   Script used for password-less ssh access to the nodes. Node is added to the _authorized_keys_ directory   |
| [`valid-configurations.json`](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/blob/main/codeco-experimentation-framework/valid_configurations.json?ref_type=heads) |   Json file used to ensure that the parameters given during the installation process match the expected values for successful deployment. **Note that this needs to be filled by the user**   |
| [`validate_input.sh`](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/blob/main/codeco-experimentation-framework/validate_input.sh?ref_type=heads) |   Script used for troubleshooting and validation of inputs when running installation scripts   |

| Directory       | Description                                                | 
|-------------------|------------------------------------------------------------|
| [`benchmarks/`](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/blob/main/codeco-experimentation-framework/benchmarks?ref_type=heads) |   Includes the [k8s-bench-suite](https://github.com/InfraBuilder/k8s-bench-suite/tree/master) benchmarking tool (KNB) |
| [`configurations/`](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/tree/main/codeco-experimentation-framework/configurations?ref_type=heads) |   Includes the configuration files used to define the cluster to be deployed i.e. master(s)/worker(s)' name, IP, MAC, the Infrastructure Manager, and credentials  |
| [`containers/`](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/tree/main/codeco-experimentation-framework/containers?ref_type=heads) |   Includes the Infrastructure Managers, DHCP server and Resource Manager   |
| [`docs/`](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/tree/main/codeco-experimentation-framework/docs?ref_type=heads) |   Includes detailed information and documentation about CODEF   |
| [`examples/`](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/tree/main/codeco-experimentation-framework/examples?ref_type=heads) |   Includes some example installation scripts e.g., ACM, L2S-M and scripts for automated experimentation about CNI plugins and K8s distributions using KNB   |
| [`results/`](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/blob/main/codeco-experimentation-framework/results?ref_type=heads) |   The destination of the results of the automated experiments located in the [examples/](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/tree/main/codeco-experimentation-framework/examples?ref_type=heads) folder  |


## Requirements
You should have the following prerequisites to use the CODEF:
1.	Install `Docker`. If `Docker` is not installed follow the [installation instructions](https://docs.docker.com/engine/install/)
2.	Install `jq`. If `jq` is not installed follow the [installation instructions](https://jqlang.github.io/jq/download/)
3.	Have **_password-less ssh access_** to the nodes you want to deploy. E.g. use the [ssh_without_password.sh](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/blob/main/codeco-experimentation-framework/ssh_without_password.sh?ref_type=heads) script to add the nodes to the _authorized_keys_ directory.

## Quickstart
1. **Clone** the repository to your local workshop e.g. node/VM that will manage the deployment of nodes, clusters and applications across your infrastructure e.g., in this case a VM with the name "boss":

```console
user@boss:~$ git clone https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework.git
user@boss:~$ cd codeco-experimentation-framework/
```

2. Ensure the necessary [**requirements**](#requirements) are met.

3. **Create** your configuration file that includes the information of your nodes (take as a template the [example-configuration](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/blob/main/codeco-experimentation-framework/configurations/example-configuration?ref_type=heads) file and **modify** it with your preferred editor):

```console
user@boss:~/codeco-experimentation-framework/configurations$ nano athena-configuration
```

4. Make sure you have updated the [valid-configurations.json](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/blob/main/codeco-experimentation-framework/valid-configurations.json?ref_type=heads) with the necessary information. This file will be used for validation of the inputs when running an installation script.

5. **Modify** and **run** an example installation script. For instance the [install-cluster.sh](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/blob/main/codeco-experimentation-framework/examples/install-cluster.sh?ref_type=heads) script:

```console
user@boss:~/codeco-experimentation-framework/examples$ nano install-cluster.sh
user@boss:~/codeco-experimentation-framework/examples$ ./install-cluster.sh
```

## Applications
The set of the custom Ansible playbooks scripts used by the _Resource Managers_ for the automated deployment of the various applications can be found in the [`playbooks`](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/tree/main/codeco-experimentation-framework/containers/resource-manager/playbooks?ref_type=heads) folder.


## Experiments
Some indicative experiments done using the CODEF including teaser videos can be found in the "Experiments" section in the [README.md](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/blob/main/codeco-experimentation-framework/docs/README.md?ref_type=heads) of the `docs/` folder.


### TO-DO
- Improve the documentation
- Resolve the issues with ACM deployment
- Add playbooks that extend the supported applications (e.g., [Thanos](https://thanos.io/) for multi-cluster monitoring, [KubeFlow](https://www.kubeflow.org/) for ML experimentation)
- Extend the experimentation metrics and tools
- **Debugging and testing from partners**
- More tests with VirtualBox Infrastructure Manager
- Start testing for multi-clustering
- **Find a way to manage the partners' access in the aws experimentation cluster**


## Contributing
TBA

## License
TBA

## Acknowledgements
Please **cite** this paper, if you use CODEF:

1. <a id="ref1"></a>G. Koukis, S. Skaperas, I. A. Kapetanidou, V. Tsaoussidis, L. Mamatas, "An Open-Source Experimentation Framework for the Edge Cloud Continuum",  INFOCOM 2024 - CNERT: 11th International Workshop on Computer and Networking Experimental Research using Testbeds [link](https://ieeexplore.ieee.org/document/10620846)

Additional papers using CODEF. These can be also found in [CODECO Zenodo](https://zenodo.org/communities/he-codeco/)

2. <a id="ref2"></a>S. Skaperas, G. Koukis, I. A. Kapetanidou, V. Tsaoussidis and L. Mamatas,   "A Pragmatical Approach to Anomaly Detection Evaluation in Edge Cloud Systems", INFOCOM 2024 - ICCN: 6th IEEE International Workshop on Intelligent Cloud Computing and Networking [link](https://ieeexplore.ieee.org/document/10620733)

3. <a id="ref3"></a>G. Koukis, S. Skaperas, I. A. Kapetanidou, L. Mamatas, V. Tsaoussidis, "Performance Evaluation of Kubernetes Networking Approaches across Constraint Edge Environments", ISCC 2024 - NGMSE: Next-Generation Multimedia Services at the Edge: Leveraging 5G and Beyond [link](https://ieeexplore.ieee.org/document/10733726)

4. <a id="ref4"></a>G. Koukis, S. Skaperas, I. A. Kapetanidou, L. Mamatas, V. Tsaoussidis, "Evaluating CNI Plugins Features & Tradeoffs for Edge Cloud Applications", ISCC 2024 - ECO: 1st International Workshop on Edge to Cloud Orchestration [link](https://ieeexplore.ieee.org/document/10733657)

Clusterslice paper that influenced CODEF:

5. <a id="ref5"></a>L. Mamatas, S. Skaperas, and I. Sakellariou, "ClusterSlice: Slicing Resources for Zero-touch Kubernetes-based Experimentation", Technical Report, University of Macedonia, https://ruomo.lib.uom.gr/handle/7000/1757, November 2023.


## Contact
Contact [Lefteris Mamatas](https://sites.google.com/site/emamatas/) from the University of Macedonia, Greece.
