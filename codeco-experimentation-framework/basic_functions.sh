#!/bin/bash

# Copyright (c) 2024 Athena RC.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0
#
# Contributors:
#      Lefteris Mamatas - author
#      George Koukis - contributor

function json_array_items () {
  # extracts items from json array
  # input is the json_array string - it should be passed with double quotes

  json_array=$1

  echo $json_array | jq -c -r '.[]'
}

function json_array_item () {
  # extracts a particular item from json array
  # inputs are the json_array string (pass it with double quotes) and the item's position

  json_array=$1
  item=$2

  echo "$json_array" | jq -c -r ".[$item]"
}

function strip_value () {
  file=$1
  field=$2

  cat $file 2> /dev/null | jq "$field"
}

function check_if_results_exist () {
   # returns true only if all results for particular deployment and replications number exist

   # first parameter is deployment type and second the replication number
   local deployment=$1
   local replicas=$2
   local experiment_name=$3

   local results_exist=true

   for ((i=1; i<=$replicas; i++))
   do
      # make it false, only if one of the results does not exist
      if [ ! -f results/${experiment_name}/${deployment}-results-$i.txt ]; then
         results_exist=false
      fi
   done
   echo $results_exist
}

# Function to check if Docker is installed
is_docker_installed() {
    if command -v docker >/dev/null 2>&1; then
        echo "true"
        return 0
    else
        echo "false"
        return 1
    fi
}

# Function to check if jq command is installed
is_jq_installed() {
    if command -v jq >/dev/null 2>&1; then
        echo "true"
        return 0
    else
        echo "false"
        return 1
    fi
}

# Function to check if a Docker container is running
is_container_running() {
    local container_name_or_id="$1"

    if docker ps --format '{{.Names}}' | grep -q "^${container_name_or_id}$"; then
        echo "true"
        return 0
    else
        echo "false"
        return 1
    fi
}

# Function to check if a Docker image is available locally
is_image_available() {
    local image_name="$1"

    if docker images --format '{{.Repository}}:{{.Tag}}' | grep -q "^${image_name}$"; then
        echo "true"
        return 0
    else
        echo "false"
        return 1
    fi
}


# Function enable variables based on the cloud_operator value
check_cloud_operator() {
    local cloud_operator="$1"

    # Set the appropriate variable to true based on the cloud_operator value
    case "$cloud_operator" in
        infrastructure-manager-xcpng)
            enable_xcpng_im=true
            ;;
        infrastructure-manager-virtualbox)
            enable_vbox_im=true
            ;;
        infrastructure-manager-cloudlab)
            enable_cloudlab_im=true
            ;;
        infrastructure-manager-aws)
            enable_aws_im=true
            ;;
        *)
            echo "Error: Invalid cloud_operator value"
            exit 1
            ;;
    esac
}
