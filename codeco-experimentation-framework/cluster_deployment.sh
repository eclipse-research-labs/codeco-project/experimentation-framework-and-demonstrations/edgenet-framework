#!/bin/bash

# Copyright (c) 2024 Athena RC.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0
#
# Contributors:
#      Lefteris Mamatas - author
#      George Koukis - contributor

# enable/disable dhcp server in docker compose 
# please note that dhcp server in docker compose cannot be automatically terminated 
# in recurring experiments. Prefer standalone execution in this case, i.e.,
# via execute_dhcp_server script. 
# Docker compose should be kept open for DHCP server to operate, while VMs that depend on it run.
enable_dhcp=false

# you can disable checking running status of DHCP
# this can work in deployments where the cloud server and
# codeco experimentation framework runs in machines in 
# different networks.
check_dhcp=true

# enable jaeger support (experimental feature)
enable_jaeger=false

# enable/disable infrastructure managers
# i.e., for XCP-NG, VirtualBox and CloudLab
# initialize all variables to false
enable_xcpng_im=false
enable_vbox_im=false
enable_cloudlab_im=false
enable_aws_im=false

# count cluster deployment
source count_cluster_deployment.sh

# check if jaeger is enabled
if $enable_jaeger; then
   echo ""
   echo "Jaeger is enabled. You can connect to hostname:16686"
   ansible_cfg=/opt/clusterslice/ansible/ansible-jaeger.cfg
else
   ansible_cfg=/opt/clusterslice/ansible/ansible.cfg
fi

echo ""
if [[ -z $cloud_operator ]]; then
   # Parameter cloud_operator is not set.
   # Check alternative master_cloud_operators and worker_cloud_operators parameters
   if [[ -z $master_cloud_operators ]] || [[ -z $worker_cloud_operators ]]; then
      "You should specify both master_cloud_operators and worker_cloud_operators arrays with valid values. Alternatively, you can specify a universal cloud operator based on the cloud_operator parameter."
      exit 1
   else
      # both master_cloud_operators and worker_cloud_operators have been set
      for entry in $(echo "$master_cloud_operators" | jq -r '.[]'); do
	 # enable appropriate IM(s)
	 check_cloud_operator $entry
      done
      for entry in $(echo "$worker_cloud_operators" | jq -r '.[]'); do
         # enable appropriate IM(s)
         check_cloud_operator $entry
      done
   fi
else
   # Selected cloud_operator is $cloud_operator
   # enable appropriate IM
   check_cloud_operator $cloud_operator
fi

echo "Enabling requested IM(s):"
echo "enable_xcpng_im is $enable_xcpng_im"
echo "enable_vbox_im is $enable_vbox_im"
echo "enable_cloudlab_im is $enable_cloudlab_im"
echo "enable_aws_im is $enable_aws_im"
echo ""

# check configuration of enabled IM(s)
if [[ $enable_xcpng_im ]] || [[ $enable_vbox_im ]]; then
   # check if dhcp server is running
   dhcp_server_is_running=$(is_container_running "dhcp-server")
fi

# check if xcp-ng IM has the appropriate configuration
if [[ $enable_xcpng_im == "true" ]]; then
   if [[ $dhcp_server_is_running == "true" ]] || [[ $check_dhcp == "false" ]]; then
      if [[ $dhcp_server_is_running == "true" ]]; then
         echo "DHCP server is running, this is needed from XCP-NG IM."
      else
	 echo "Disabled checking running status of DHCP, this is fine."
      fi
      echo "Check if docker image for XCP-NG IM is available locally."
      is_xcpng_image_available=$(is_image_available "swnuom/infrastructure-manager-xcpng:latest")
      if [[ $is_xcpng_image_available == "true" ]]; then
	 echo "Docker image swnuom/infrastructure-manager-xcpng is available."
      else
	 echo "Docker image swnuom/infrastructure-manager-xcpng is not available."
	 echo "Compiling image."
         cd containers/infrastructure-manager-xcpng
	 ./update_image
	 cd ../../
      fi
   else
      echo "DHCP server is not running, so XCP-NG IM cannot work."
      echo "Please configure and execute DHCP server based on dhcp/example-dhcpd.conf file."
      echo "After configuring dhcpd.conf, you can run dhcp/execute_dhcp_server.sh."
      echo ""
      exit 1
   fi

fi

# check if virtualbox IM has the appropriate configuration
if [[ $enable_vbox_im == "true" ]]; then
   if [[ $dhcp_server_is_running == "true" ]] || [[ $check_dhcp == "false" ]]; then
      if [[ $dhcp_server_is_running == "true" ]]; then
         echo "DHCP server is running, this is needed from XCP-NG IM."
      else
         echo "Disabled checking running status of DHCP, this is fine."
      fi
      echo "Check if docker image for virtualbox IM is available locally."
      is_vbox_image_available=$(is_image_available "swnuom/infrastructure-manager-virtualbox:latest")
      if [[ $is_vbox_image_available == "true" ]]; then
         echo "Docker image swnuom/infrastructure-manager-virtualbox is available."
      else
         echo "Docker image swnuom/infrastructure-manager-virtualbox is not available."
         echo "Compiling image."
         cd containers/infrastructure-manager-virtualbox
         ./update_image
         cd ../../
      fi
   else
      echo "DHCP server is not running, so virtualbox IM cannot work."
      echo "Please configure and execute DHCP server based on dhcp/example-dhcpd.conf file."
      echo "After configuring dhcpd.conf, you can run dhcp/execute_dhcp_server.sh."
      echo ""
      exit 1
   fi

fi

# check if aws IM has the appropriate configuration
if [[ $enable_aws_im == "true" ]]; then
  echo "Check if docker image for aws IM is available locally."
  is_aws_image_available=$(is_image_available "swnuom/infrastructure-manager-aws:latest")
  if [[ $is_aws_image_available == "true" ]]; then
     echo "Docker image swnuom/infrastructure-manager-aws is available."
  else
     echo "Docker image swnuom/infrastructure-manager-aws is not available."
     echo "Compiling image."
     cd containers/infrastructure-manager-aws
     ./update_image
     cd ../../
  fi
fi

echo ""
echo "Check if resource-manager image is available locally."
is_resource_manager_image_available=$(is_image_available "swnuom/resource-manager:latest")
if [[ ${is_resource_manager_image_available} == "true" ]]; then
   echo "Docker image swnuom/resource-manager is available."
else
   echo "Docker image swnuom/resource-manager is not available."
   echo "Compiling image."
   cd containers/resource-manager
   ./update_image
   cd ../../
fi

echo ""

# function that generates docker-compose.yaml
function create_docker_compose_yaml () {
  # input parameters
  local k8s_type=$kubernetes_type
  local k8s_networkfabric=$kubernetes_networkfabric
  # escape single quote to double quote
  local k8s_networkfabricparameters="${kubernetes_networkfabricparameters//[\']/'\"'}"

  local k8s_networkcidr=$kubernetes_networkcidr
  local k8s_servicecidr=$kubernetes_servicecidr
  local k8s_version=$kubernetes_version
  local k8s_containerd_version=$containerd_version
  local k8s_critools_version=$critools_version

  # generate file

# it seems version tag is obsolete, commenting this out
#cat > docker-compose.yaml << EOF
#version: '3.8'
#EOF

cat > docker-compose.yaml << EOF
services:
EOF

# add dhcp service, if it is enabled
if $enable_dhcp; then
cat >> docker-compose.yaml << EOF
  dhcp-server:
    image: swnuom/dhcp-server
    volumes:
      - ~/codeco-experiments/dhcp/:/etc/dhcp/
    network_mode: host
    restart: unless-stopped

EOF
fi

# add jaeger service, if it is enabled
if $enable_jaeger; then
cat >> docker-compose.yaml << EOF
  jaeger:
    image: jaegertracing/all-in-one:1.35
    environment:
      - COLLECTOR_OTLP_ENABLED=true
    #network_mode: host
    restart: no
    ports:
      - "16686:16686"  # Jaeger UI
      - "4317:4317"    # OTLP gRPC
      - "4318:4318"    # OTLP HTTP
EOF
fi

# add XCP-NG infrastructure manager, if it is enabled
if $enable_xcpng_im; then
cat >> docker-compose.yaml << EOF
  infrastructure-manager-xcpng:
    image: swnuom/infrastructure-manager-xcpng
    volumes:
      - ~/.ssh:/etc/ssh/
      - xcpng-im:/root/
    network_mode: host
    restart: no
EOF
fi

# add VirtualBox infrastructure manager, if it is enabled
if $enable_vbox_im; then
cat >> docker-compose.yaml << EOF
  infrastructure-manager-virtualbox:
    image: swnuom/infrastructure-manager-virtualbox
    volumes:
      - ~/.ssh:/etc/ssh/
      - vbox-im:/root/
    network_mode: host
    restart: no
EOF
fi

# add AWS infrastructure manager, if it is enabled
if $enable_aws_im; then
cat >> docker-compose.yaml << EOF
  infrastructure-manager-aws:
    image: swnuom/infrastructure-manager-aws
    volumes:
      - ~/.ssh:/etc/ssh/
      - aws-im:/root/
      - ~/.aws/:/root/.aws/
    network_mode: host
    restart: no
EOF
fi

# add CloudLab infrastructure manager, if it is enabled
if $enable_cloudlab_im; then
cat >> docker-compose.yaml << EOF
  infrastructure-manager-cloudlab:
    image: swnuom/infrastructure-manager-cloudlab
    volumes:
      - ~/.ssh:/etc/ssh/
      - cloudlab-im:/root/
    network_mode: host
    restart: no
EOF
fi

# iterate through master nodes
count=0
hosts=`json_array_items "$master_hosts"`
for node in $hosts;
do
  ip=$(json_array_item "$master_ips" $count)
  mac=$(json_array_item "$master_macs" $count)

cat >> docker-compose.yaml << EOF
  $node:
    image: swnuom/resource-manager
    depends_on:
EOF

if $enable_xcpng_im; then
cat >> docker-compose.yaml << EOF
      - infrastructure-manager-xcpng
EOF
fi
if $enable_vbox_im; then
cat >> docker-compose.yaml << EOF
      - infrastructure-manager-virtualbox
EOF
fi
if $enable_aws_im; then
cat >> docker-compose.yaml << EOF
      - infrastructure-manager-aws
EOF
fi
if $enable_cloudlab_im; then
cat >> docker-compose.yaml << EOF
      - infrastructure-manager-cloudlab
EOF
fi

cat >> docker-compose.yaml << EOF
    volumes:
      - ~/.ssh:/etc/ssh/
      - shared:/opt/clusterslice/shared/
      - /var/run/docker.sock:/var/run/docker.sock
EOF

# enable im volume mount, if enabled
if $enable_xcpng_im; then
cat >> docker-compose.yaml << EOF
      - xcpng-im:/opt/clusterslice/infrastructure-manager-xcpng/
EOF
fi
if $enable_vbox_im; then
cat >> docker-compose.yaml << EOF
      - vbox-im:/opt/clusterslice/infrastructure-manager-virtualbox/
EOF
fi
if $enable_aws_im; then
cat >> docker-compose.yaml << EOF
      - aws-im:/opt/clusterslice/infrastructure-manager-aws/
EOF
fi
if $enable_cloudlab_im; then
cat >> docker-compose.yaml << EOF
      - cloudlab-im:/opt/clusterslice/infrastructure-manager-cloudlab/
EOF
fi

cat >> docker-compose.yaml << EOF
      - ~/codeco-experiments/benchmarks/:/opt/clusterslice/playbooks/files/benchmarks/
      - ~/codeco-experiments/results/:/opt/clusterslice/results/
    command: /bin/bash -c /opt/clusterslice/start.sh
    environment:
      - K8S=false
      - ADMIN_USERNAME=$admin_username
      - ADMIN_PASSWORD=$admin_password
EOF

# check if host os account is being specified
if [[ ! -z $node_osaccount ]]; then
  cat >> docker-compose.yaml << EOF
      - NODE_OSACCOUNT=$node_osaccount
EOF
fi

# check if there are individual node cloud configurations for master nodes or not
if [[ -z $master_cloud_names ]]; then
  cat >> docker-compose.yaml << EOF
      - CLOUD_IP=$cloud_ip
      - CLOUD_NAME=$cloud_name
      - CLOUD_OPERATOR=$cloud_operator
EOF
else
  individual_cloud_name=$(json_array_item "$master_cloud_names" $count)
  individual_cloud_ip=$(json_array_item "$master_cloud_ips" $count)
  individual_cloud_operator=$(json_array_item "$master_cloud_operators" $count)
  cat >> docker-compose.yaml << EOF
      - CLOUD_IP=$individual_cloud_ip
      - CLOUD_NAME=$individual_cloud_name
      - CLOUD_OPERATOR=$individual_cloud_operator
EOF
fi

cat >> docker-compose.yaml << EOF
      - KUBERNETES_TYPE=$k8s_type
      - KUBERNETES_NETWORKFABRIC=$k8s_networkfabric
      - KUBERNETES_NETWORKFABRICPARAMETERS=$k8s_networkfabricparameters
      - KUBERNETES_NETWORKCIDR=$k8s_networkcidr
      - KUBERNETES_SERVICECIDR=$k8s_servicecidr
      - KUBERNETES_VERSION=$k8s_version
      - CONTAINERD_VERSION=$k8s_containerd_version
      - CRITOOLS_VERSION=$k8s_critools_version
      - NODE_NAME=$node
      - NODE_IP=$ip
      - NODE_TYPE=mastervm
      - NODE_OSIMAGE=$node_osimage
      - NODE_MAC=$mac
      - MASTER_IPS=$master_ips
      - MASTER_HOSTS=$master_hosts
      - WORKER_HOSTS=$worker_hosts
      - WORKER_IPS=$worker_ips
      - APP_NAMES=$app_names
      - APP_SCOPES=$app_scopes
      - APP_VERSIONS=$app_versions
      - ANSIBLE_CONFIG=$ansible_cfg
      - OTEL_EXPORTER_OTLP_ENDPOINT=http://jaeger:4317
      - OTEL_SERVICE_NAME=ansible  
      - ANSIBLE_OPENTELEMETRY_ENABLED=true
      - CLUSTER_NUMBER=$cluster_number
EOF
  let count=count+1
done

# iterate through worker nodes
count=0
hosts=`json_array_items "$worker_hosts"`
for node in $hosts;
do
  ip=`json_array_item "$worker_ips" $count`
  mac=`json_array_item "$worker_macs" $count`

cat >> docker-compose.yaml << EOF
  $node:
    image: swnuom/resource-manager
    depends_on:
EOF
if $enable_xcpng_im; then
cat >> docker-compose.yaml << EOF
      - infrastructure-manager-xcpng
EOF
fi
if $enable_vbox_im; then
cat >> docker-compose.yaml << EOF
      - infrastructure-manager-virtualbox
EOF
fi
if $enable_aws_im; then
cat >> docker-compose.yaml << EOF
      - infrastructure-manager-aws
EOF
fi
if $enable_cloudlab_im; then
cat >> docker-compose.yaml << EOF
      - infrastructure-manager-cloudlab
EOF
fi

cat >> docker-compose.yaml << EOF
    volumes:
      - ~/.ssh:/etc/ssh/
      - shared:/opt/clusterslice/shared/
      - /var/run/docker.sock:/var/run/docker.sock
EOF

if $enable_xcpng_im; then
cat >> docker-compose.yaml << EOF
      - xcpng-im:/opt/clusterslice/infrastructure-manager-xcpng/
EOF
fi
if $enable_vbox_im; then
cat >> docker-compose.yaml << EOF
      - vbox-im:/opt/clusterslice/infrastructure-manager-virtualbox/
EOF
fi
if $enable_aws_im; then
cat >> docker-compose.yaml << EOF
      - aws-im:/opt/clusterslice/infrastructure-manager-aws/
EOF
fi
if $enable_cloudlab_im; then
cat >> docker-compose.yaml << EOF
      - cloudlab-im:/opt/clusterslice/infrastructure-manager-cloudlab/
EOF
fi

cat >> docker-compose.yaml << EOF
      - ~/codeco-experiments/benchmarks/:/opt/clusterslice/playbooks/files/benchmarks/
      - ~/codeco-experiments/results/:/opt/clusterslice/results/
    command: /bin/bash -c /opt/clusterslice/start.sh
    environment:
      - K8S=false
      - ADMIN_USERNAME=$admin_username
      - ADMIN_PASSWORD=$admin_password
EOF

# check if host os account is being specified
if [[ ! -z $node_osaccount ]]; then
  cat >> docker-compose.yaml << EOF
      - NODE_OSACCOUNT=$node_osaccount
EOF
fi

# check if there are individual node cloud configurations for worker nodes or not
if [[ -z $worker_cloud_names ]]; then
  cat >> docker-compose.yaml << EOF
      - CLOUD_IP=$cloud_ip
      - CLOUD_NAME=$cloud_name
      - CLOUD_OPERATOR=$cloud_operator
EOF
else
  individual_cloud_name=$(json_array_item "$worker_cloud_names" $count)
  individual_cloud_ip=$(json_array_item "$worker_cloud_ips" $count)
  individual_cloud_operator=$(json_array_item "$worker_cloud_operators" $count)
  cat >> docker-compose.yaml << EOF
      - CLOUD_IP=$individual_cloud_ip
      - CLOUD_NAME=$individual_cloud_name
      - CLOUD_OPERATOR=$individual_cloud_operator
EOF
fi

cat >> docker-compose.yaml << EOF
      - KUBERNETES_TYPE=$k8s_type
      - KUBERNETES_NETWORKFABRIC=$k8s_networkfabric
      - KUBERNETES_NETWORKFABRICPARAMETERS=$k8s_networkfabricparameters
      - KUBERNETES_NETWORKCIDR=$k8s_networkcidr
      - KUBERNETES_SERVICECIDR=$k8s_servicecidr
      - KUBERNETES_VERSION=$k8s_version
      - CONTAINERD_VERSION=$k8s_containerd_version
      - CRITOOLS_VERSION=$k8s_critools_version
      - NODE_NAME=$node
      - NODE_IP=$ip
      - NODE_TYPE=workervm
      - NODE_OSIMAGE=$node_osimage
      - NODE_MAC=$mac
      - MASTER_IPS=$master_ips
      - MASTER_HOSTS=$master_hosts
      - WORKER_HOSTS=$worker_hosts
      - WORKER_IPS=$worker_ips
      - APP_NAMES=$app_names
      - APP_SCOPES=$app_scopes
      - APP_VERSIONS=$app_versions
      - ANSIBLE_CONFIG=$ansible_cfg
      - OTEL_EXPORTER_OTLP_ENDPOINT=http://jaeger:4317
      - OTEL_SERVICE_NAME=ansible  
      - ANSIBLE_OPENTELEMETRY_ENABLED=true
      - CLUSTER_NUMBER=$cluster_number
EOF
  let count=count+1
done

# add reference to shared volume
cat >> docker-compose.yaml << EOF
volumes:
  shared: {}
EOF

if $enable_xcpng_im; then
cat >> docker-compose.yaml << EOF
  xcpng-im: {}
EOF
fi
if $enable_vbox_im; then
cat >> docker-compose.yaml << EOF
  vbox-im: {}
EOF
fi
if $enable_aws_im; then
cat >> docker-compose.yaml << EOF
  aws-im: {}
EOF
fi
if $enable_cloudlab_im; then
cat >> docker-compose.yaml << EOF
  cloudlab-im: {}
EOF
fi

}

# cleanup
echo "*** Cleaning up ***"
docker compose down --volumes --remove-orphans

# Pulling latest version of resource manager
#echo ""
#echo "*** Pulling latest stable version of containers ***"
#docker pull swnuom/resource-manager
#if $enable_xcpng_im; then
#  docker pull swnuom/infrastructure-manager-xcpng
#fi
#if $enable_vbox_im; then
#docker pull swnuom/infrastructure-manager-virtualbox
#fi
#if $enable_aws_im; then
#docker pull swnuom/infrastructure-manager-aws
#fi
#if $enable_cloudlab_im; then
#  docker pull swnuom/infrastructure-manager-cloudlab
#fi

# generate docker-compose.yaml
echo ""
echo "*** Generating docker-compose.yaml ***"
create_docker_compose_yaml 

# execute docker compose to deploy the cluster
echo ""
echo "*** Executing docker compose ***"
docker compose up 

# Check if Ctrl+C (SIGINT) was received
if [ $? -eq 130 ]; then
   echo "Ctrl+C (SIGINT) was received."
   # Add cleanup or other actions as needed
   exit 1
fi
