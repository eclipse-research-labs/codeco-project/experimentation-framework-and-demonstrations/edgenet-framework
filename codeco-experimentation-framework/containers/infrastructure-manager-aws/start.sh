#!/bin/bash

# Copyright (c) 2024 Athena RC.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0
#
# Contributors:
#      Lefteris Mamatas - author
#      George Koukis - contributor

# Function to clean up and gracefully exit
cleanup() {
    echo "The Infrastructure Manager received a termination signal."
    exit 0
}

# Trap termination signals and call the cleanup function
trap 'cleanup' SIGTERM SIGINT

# configure ssh keys
source /root/configure_ssh.sh

# implement endless loop
while true; do
    # terminate in the case of "completed" file appears in /root/, this means the non-k8s deployment is completed
    if [ -f "/root/completed" ]; then
      echo "Triggered termination signal"
      exit 0
    fi
    sleep 3
done
