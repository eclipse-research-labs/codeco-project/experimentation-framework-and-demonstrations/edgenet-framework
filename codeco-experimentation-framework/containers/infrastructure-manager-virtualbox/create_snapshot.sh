#!/bin/bash

# Copyright (c) 2024 Athena RC.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0
#
# Contributors:
#      Lefteris Mamatas - author
#      George Koukis - contributor

# script that creates a new virtual machine snapshot
# input variables are cloud_server, vm_uuid and vm_name (it is the same with snapshot_name)

# check the number of arguments passed
if [ "$#" != "3" ]; then
  echo "Illegal number of parameters passed. The correct syntax is:"
  echo "create_snapshot.sh cloud_server vm_uuid and vm_name (it will be the same with snapshot_name)"
  exit 1
fi

# function input variables
server=$1
vm_uuid=$2
vm=$3

# import configuration, if it is not imported (standalone execution)
source /root/import_configuration.sh

log_output "check if snapshot already exists."
snapshot_uuid=$(source /root/get_vm_snapshot_uuid.sh $server $vm)

if [[ ! -z $snapshot_uuid ]]; then
  if [[ $force_resource_removal == true ]]; then
    log_output "snapshot already exists, deleting existing snapshot."
    $VBoxManage snapshot delete $vm --uuid $snapshot_uuid >/dev/null 2>/dev/null
    if [ $? -ne 0 ]; then
      log_output "cannot remove snapshot."
      exit 1
    fi
    log_output "snapshot removed"
  else
    log_output "snapshot already exists."
    exit 1
  fi
fi

# creating a new snapshot
$VBoxManage snapshot $vm_uuid take $vm >/dev/null 2>/dev/null
if [ $? -ne 0 ]; then
  exit 1
fi
