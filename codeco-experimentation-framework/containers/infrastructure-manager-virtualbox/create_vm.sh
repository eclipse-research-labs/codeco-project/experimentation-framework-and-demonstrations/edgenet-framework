#!/bin/bash

# Copyright (c) 2024 Athena RC.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0
#
# Contributors:
#      Lefteris Mamatas - author
#      George Koukis - contributor

# script that creates a new virtual machine based on a particular template
# input variables are cloud_name, vm, template
# it uses the configuration variable sr_uuid
# it returns vm_uuid

# check the number of arguments passed
if [ "$#" != "3" ]; then
  echo "Illegal number of parameters passed. The correct syntax is:"
  echo "create_vm.sh cloud_server vm_name vm_template"
  exit 1
fi

# function input variables
server=$1
vm=$2
template=$3

# import configuration, if it is not imported (standalone execution)
source /root/import_configuration.sh

# create and name VM with a single command
$VBoxManage import $image_path/${template}.ova --vsys 0 --vmname $vm 2>/dev/null >/dev/null

if [ $? -ne 0 ]; then
  log_output "failure to create VM."
  exit 1
fi
log_output "created VM."

# retrieve and return VM uuid
source /root/get_vm_uuid.sh $server $vm
