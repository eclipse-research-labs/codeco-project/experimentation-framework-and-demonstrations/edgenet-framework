#!/bin/bash

# Copyright (c) 2024 Athena RC.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0
#
# Contributors:
#      Lefteris Mamatas - author
#      George Koukis - contributor

# executes DHCP server
# should be run in the virtualbox server

# use image prefix, if it exists
if [[ -f "image_prefix" ]]; then
  image_prefix=$(cat image_prefix)
else
  image_prefix="swnrg"
fi

# remove existing clusterslice-dhcp, in the case it exists
docker stop clusterslice-dhcp 2> /dev/null
docker remove clusterslice-dhcp 2> /dev/null

docker run -d --name clusterslice-dhcp --net=host $image_prefix/clusterslice-dhcp
