#!/bin/bash

# Copyright (c) 2024 Athena RC.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0
#
# Contributors:
#      Lefteris Mamatas - author
#      George Koukis - contributor

srs=("coralone_local_storage" "coralone_local_storage_2" "coraltwo_local_storage" "coraltwo_local_storage_2")

uuids=("3c5cb115-15bf-2ab5-5914-91ddfe3fba4a" "07db1682-8f12-3e74-46d4-3591f72b99f4" "6ca3994c-2e47-28c0-3c0a-355ab14e5fa7" "437f46a9-1969-c03b-2664-0f30abd44e96")

sruuid=$1

function get_free_space () {
  result=$(xe sr-list uuid=$sruuid params=physical-utilisation --minimal)
  total=$(xe sr-list uuid=$sruuid params=physical-size --minimal)
  echo "scale=2; $result / $total" | bc | xargs printf "%.2f\n"
}

if [[ -z $sruuid ]]; then
  # no parameter passed, show all SRs
  for arrayindex in ${!srs[@]};
  do
    sr=${srs[$arrayindex]}
    sruuid=${uuids[$arrayindex]}
    echo "$sr $sruuid"
    get_free_space
  done
else
  get_free_space
fi
