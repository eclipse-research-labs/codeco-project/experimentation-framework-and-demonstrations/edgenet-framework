# Copyright (c) 2024 Athena RC.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0
#
# Contributors:
#      Lefteris Mamatas - author
#      George Koukis - contributor

import time
import os
from ansible.plugins.callback import CallbackBase
from datetime import timedelta

class CallbackModule(CallbackBase):
    """
    Custom callback plugin to calculate execution time and track task status.
    """
    CALLBACK_VERSION = 1.0
    CALLBACK_TYPE = 'notification'
    CALLBACK_NAME = 'codeco_plugin'

    def __init__(self):
        super(CallbackModule, self).__init__()
        self.task_start_time = None
        self.task_stats = {}
        self.processed_tasks = set()  # To track processed tasks and avoid duplicates
        self.experiment_name = os.getenv('EXPERIMENT_NAME', '')
        self.node_name = os.getenv('NODE_NAME', '')
        self.cluster_number = os.getenv('CLUSTER_NUMBER', '')
        self.logfile = f'/opt/clusterslice/results/{self.experiment_name}/{self.node_name}_{self.cluster_number}_output.txt'
        self._ensure_log_directory()

    def _ensure_log_directory(self):
        # Ensure the results directory exists, create if not
        log_dir = os.path.dirname(self.logfile)
        if not os.path.exists(log_dir):
            os.makedirs(log_dir)

    def _write_to_logfile(self, content):
        # Write content to the logfile
        with open(self.logfile, 'a', buffering=1) as log_file:
            log_file.write(content + '\n')

    def v2_playbook_on_task_start(self, task, is_conditional):
        # Record task start time without logging
        self.task_start_time = time.time()
        task_name = task.get_name().strip()
        #self._display.display(f"Task '{task_name}' is being executed.", color='cyan')

    def v2_runner_on_ok(self, result):
        task_name = result.task_name
        task_host = result._host  # Get the host for uniqueness

        # Create a unique key for this task based on name and host
        task_key = f"{task_name}_{task_host}"

        # Check if this task has been processed before
        if task_key not in self.processed_tasks:
            task_duration = self._get_task_duration()
            self._log_task_result(task_name, "SUCCESS", task_duration, task_host)
            self.processed_tasks.add(task_key)  # Mark task as processed

    def v2_runner_on_failed(self, result, ignore_errors=False):
        task_name = result.task_name
        task_host = result._host  # Get the host for uniqueness

        # Create a unique key for this task based on name and host
        task_key = f"{task_name}_{task_host}"

        # Check if this task has been processed before
        if task_key not in self.processed_tasks:
            task_duration = self._get_task_duration()
            self._log_task_result(task_name, "FAILED", task_duration, task_host)
            self.processed_tasks.add(task_key)  # Mark task as processed

    def v2_runner_on_skipped(self, result):
        task_name = result.task_name
        task_host = result._host  # Get the host for uniqueness

        # Create a unique key for this task based on name and host
        task_key = f"{task_name}_{task_host}"

        # Check if this task has been processed before
        if task_key not in self.processed_tasks:
            task_duration = self._get_task_duration()
            self._log_task_result(task_name, "SKIPPED", task_duration, task_host)
            self.processed_tasks.add(task_key)  # Mark task as processed

    def v2_playbook_on_stats(self, stats):
        # Output the summary of all task execution times
        self._display.display("Playbook execution complete.", color='blue')
        for task_name, info in self.task_stats.items():
            status = info["status"]
            host = info["host"]
            duration = str(timedelta(seconds=info["duration"]))
            self._display.display(f"Task '{task_name}': {status} - Duration: {duration}", color='green' if status == 'SUCCESS' else 'red')
            # Keep statistics in log file
            log_entry = f"{host}, {task_name}, {status}, {duration}"
            # Write log entry to the logfile
            self._write_to_logfile(log_entry)
    def _get_task_duration(self):
        """
        Helper function to calculate the duration of the task
        """
        return time.time() - self.task_start_time

    def _log_task_result(self, task_name, status, duration, host):
        """
        Helper function to log the result and duration of the task
        """
        self.task_stats[task_name] = {
            "status": status,
            "duration": duration,
            "host": host
        }
        self._display.display(f"Task '{task_name}' {status}. Duration: {timedelta(seconds=duration)}", color='green' if status == 'SUCCESS' else 'red')
