#!/bin/bash

# Copyright (c) 2024 Athena RC.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0
#
# Contributors:
#      Lefteris Mamatas - author
#      George Koukis - contributor

function benchmark_slice () {
  local slice_name=$1
  local slice_status=$2
  local resource_name=$3
  local resource_status=$4

  current_time=`date +%s%N`
  if [[ $slice_status == "requested" ]]; then
     rm /tmp/${slice_name}_benchmarking.txt 2> /dev/null
     echo $current_time > /tmp/${slice_name}_start_time.txt
     #if [ ! -f /tmp/${slice_name}_start_time.txt ]; then
       #rm /tmp/${slice_name}_benchmarking.txt 2> /dev/null
       #echo $current_time > /tmp/${slice_name}_start_time.txt
     #fi
  fi
 
  # if start_time.txt exists, track measurement
  if [ -f "/tmp/${slice_name}_start_time.txt" ]; then
    start_time=`cat /tmp/${slice_name}_start_time.txt`
    elapsed_time=`expr $current_time - $start_time`
    echo "$slice_name, $slice_status, $resource_name, $resource_status, $elapsed_time" >> /tmp/${slice_name}_benchmarking.txt
  fi
}
