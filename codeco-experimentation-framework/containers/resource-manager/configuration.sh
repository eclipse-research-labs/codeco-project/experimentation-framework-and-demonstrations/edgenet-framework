#!/bin/bash

# Copyright (c) 2024 Athena RC.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0
#
# Contributors:
#      Lefteris Mamatas - author
#      George Koukis - contributor

# defines main path
main_path=/opt/clusterslice/

# define hosts file name and playbook directory
hostsfile=$main_path/ansible/hosts
playbook_path=$main_path/playbooks

# define test-bed namespace (i.e., for slicerequest & slice operators as well as computeresources) 
testbed_namespace="swn"

# define ansible trailer
ansible_trailer="ansible_ssh_host=ip ansible_ssh_port=22 ansible_ssh_user=user"
ansible_debug="" #-vvvvv" #-vvv

# one can disable it and create a docker container out of resource-manager
if [ -z "$K8S" ]; then
   k8s=true
else
   k8s=$K8S
fi
