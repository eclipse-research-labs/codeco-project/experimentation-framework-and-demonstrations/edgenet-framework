#!/bin/bash
## This script is executed after the CODECO operator deployment finishes. It is used to install additional
## components and to perform any other post-deployment tasks.
echo "Executing post deployment tasks..."
##TODO(user): Add your post deployment tasks here
cd ..
echo "........................................Installing NetMA..............................................."
cd secure-connectivity

# remove taint from master node
kubectl taint nodes athm1 node-role.kubernetes.io/control-plane:NoSchedule-
# kubectl taint nodes --all node-role.kubernetes.io/control-plane- node-role.kubernetes.io/master-

# label master node
kubectl label nodes athm1 dedicated=control-plane

# install multus
#kubectl apply -f https://raw.githubusercontent.com/k8snetworkplumbingwg/multus-cni/master/deployments/multus-daemonset-thick.yml
#cat ../multus-cni/deployments/multus-daemonset-thick.yml | kubectl apply -f -

# install cert-manager
kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.15.2/cert-manager.yaml

# install l2sm
kubectl create -f ./deployments/l2sm-deployment.yaml

# wait for all pods to be ready
KUBECONFIG=~/.kube/config kubectl wait --for=condition=Ready pods --all -n he-codeco-netma --timeout=600s

# create vxlans
$HOME/acm/scripts/create_vxlans.sh

cd ..
echo "........................................Finished installing NetMA..............................................."

echo ".....................Installing MDM....................................."
cd mdm-api
export MDM_NAMESPACE=he-codeco-mdm
export MDM_CONTEXT=standalone-context
kubectl --context=$MDM_CONTEXT create namespace $MDM_NAMESPACE
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo add neo4j https://helm.neo4j.com/neo4j
sed -i "s/<storageclassName>/standard/g" "./deployment/zookeeper-helm.yaml"
sed -i "s/storageClass: \"\"/storageClass: \"standard\"/g" "./deployment/kafka-helm.yaml"
sed -i "s/<storageclassName>/standard/g" "./deployment/neo4j-helm.yaml"
helm --kube-context=$MDM_CONTEXT install mdm-zookeeper -n $MDM_NAMESPACE  bitnami/zookeeper  -f ./deployment/zookeeper-helm.yaml --set volumePermissions.enabled=true
helm --kube-context=$MDM_CONTEXT install mdm-kafka -n $MDM_NAMESPACE bitnami/kafka --version 21.1.1 -f ./deployment/kafka-helm.yaml --set volumePermissions.enabled=true
# fix neo4j permissions - run as root
echo ""  >> ./deployment/neo4j-helm.yaml
echo "containerSecurityContext:" >> ./deployment/neo4j-helm.yaml
echo "  runAsNonRoot: false" >> ./deployment/neo4j-helm.yaml
echo "  runAsUser: 0" >> ./deployment/neo4j-helm.yaml
echo "  runAsGroup: 0" >> ./deployment/neo4j-helm.yaml
helm --kube-context=$MDM_CONTEXT install mdm-neo4j -n $MDM_NAMESPACE neo4j/neo4j-standalone -f ./deployment/neo4j-helm.yaml 
kubectl --context=$MDM_CONTEXT -n $MDM_NAMESPACE exec -i mdm-kafka-0 -- /opt/bitnami/kafka/bin/kafka-topics.sh --bootstrap-server mdm-kafka-0:9093 --create --topic json-events --config cleanup.policy=compact
helm --kube-context=$MDM_CONTEXT -n $MDM_NAMESPACE install mdm-controller ./controller/src/helm -f ./controller/src/helm/values.yaml
helm --kube-context=$MDM_CONTEXT -n $MDM_NAMESPACE install mdm-api ./mdm-api/src/helm -f ./mdm-api/src/helm/values.yaml

# wait for all pods to be ready
KUBECONFIG=~/.kube/config kubectl wait --for=condition=Ready pods --all -n he-codeco-netma --timeout=600s

# MDM update
cd ..
cd mdm-connectors
sed -i "s/<namespace>/$MDM_NAMESPACE/g" ./connectors/k8s/src/main/helm/values.yaml
sed -i "s/<clustername>/$MDM_CONTEXT/g" ./connectors/k8s/src/main/helm/values.yaml
sed -i "s/<namespace>/$MDM_NAMESPACE/g" ./connectors/kubescape/src/main/helm/values.yaml
sed -i "s/<clustername>/$MDM_CONTEXT/g" ./connectors/kubescape/src/main/helm/values.yaml
sed -i "s/<namespace>/$MDM_NAMESPACE/g" ./connectors/prometheus/src/main/helm/values.yaml
sed -i "s/<clustername>/$MDM_CONTEXT/g" ./connectors/prometheus/src/main/helm/values.yaml
# Prometheus config set the Prometheus url to $PROMETHEUS_URL
export PROMETHEUS_URL=http://prometheus-k8s.monitoring.svc.cluster.local
# Prometheus config set the Prometheus port to $PROMETHEUS_PORT
export PROMETHEUS_PORT=9090
sed -i "s|http://prometheus-service.monitoring.svc.cluster.local|$PROMETHEUS_URL|g" ./connectors/prometheus/src/main/helm/values.yaml
sed -i "s/9090/$PROMETHEUS_PORT/g" ./connectors/prometheus/src/main/helm/values.yaml
helm --kube-context=$MDM_CONTEXT -n $MDM_NAMESPACE install k8s-connector ./connectors/k8s/src/main/helm -f ./connectors/k8s/src/main/helm/values.yaml 
helm --kube-context=$MDM_CONTEXT -n $MDM_NAMESPACE install kubescape-connector ./connectors/kubescape/src/main/helm -f ./connectors/kubescape/src/main/helm/values.yaml 
helm --kube-context=$MDM_CONTEXT -n $MDM_NAMESPACE install freshness-connector ./connectors/prometheus/src/main/helm -f ./connectors/prometheus/src/main/helm/values.yaml
cd ..
echo "........................................Finished installing MDM..............................................."
# export POD_NAME=$(sudo kubectl get pods --namespace mdm -l "app.kubernetes.io/name=mdm-api,app.kubernetes.io/instance=mdm-api" -o jsonpath="{.items[0].metadata.name}")
# export CONTAINER_PORT=$(sudo kubectl get pod --namespace mdm $POD_NAME -o jsonpath="{.spec.containers[0].ports[0].containerPort}")
# sudo kubectl get all -n codecoapp-operator-system
# sudo kubectl get nodes
# sudo kubectl get networklinks -A
# sudo kubectl get networkpaths -A
# sudo kubectl --namespace mdm port-forward $POD_NAME 9092:$CONTAINER_PORT
echo ".....................Installing PDLC....................................."

# Prometheus installation

cd kube-prometheus
kubectl apply --server-side -f manifests/setup
kubectl wait \
	--for condition=Established \
	--all CustomResourceDefinition \
	--namespace=monitoring
kubectl apply -f manifests/
cd ..

#Data generator
cd synthetic-data-generator

#git checkout main-hotfixed

# update configuration to the particular cluster
export CLUSTER_NODES="athm1,athw1,athw2"   # "control-plane-1,worker-1,worker-2"

sed -i "s/value: \"node1,node2,node3\"/value: \"$CLUSTER_NODES\"/g" ./netma-controller/netma-controller-deployment.yaml
sed -i "s/value: \"node1,node2,node3\"/value: \"$CLUSTER_NODES\"/g" ./acm-controller/acm-controller-deployment.yaml

#chmod -R 777 apply-controllers.sh
./apply-controllers.sh

# deploy dummy application
./apply-dummy.sh

cd ..

#PDLC
cd pdlc-integration

# Updating configuration to particular cluster
export CLUSTER_NAME=codecocloud #sonem data_preprocessing/pdlc-dp-deployment.yaml
export NODE_NUMBER=3 #value: "3" rl_model/rl-model-deployment.yaml
export DP_NODE=athw1 #sonem-worker data_preprocessing/pdlc-dp-deployment.yaml
export CA_NODE=athw1 #sonem-worker context_awareness/pdlc-ca-deployment.yaml
export GNNC_NODE=athw2 #sonem-worker gnn_model/gnn_controller.yaml
export GNNI_NODE=athw2 #sonem-worker gnn_model/gnn_inference.yaml
export RLM_NODE=athw2 #sonem-worker rl_model/rl-model-deployment.yaml

sed -i "s/value: \"3\"/value: \"$NODE_NUMBER\"/g" ./rl_model/rl-model-deployment.yaml
sed -i "s/sonem-worker/$DP_NODE/g" ./data_preprocessing/pdlc-dp-deployment.yaml
sed -i "s/sonem/$CLUSTER_NAME/g" ./data_preprocessing/pdlc-dp-deployment.yaml
sed -i "s/sonem-worker/$CA_NODE/g" ./context_awareness/pdlc-ca-deployment.yaml
sed -i "s/sonem-worker/$GNNC_NODE/g" ./gnn_model/gnn_controller.yaml
sed -i "s/sonem-worker/$GNNI_NODE/g" ./gnn_model/gnn_inference.yaml
sed -i "s/sonem-worker/$RLM_NODE/g" ./rl_model/rl-model-deployment.yaml

chmod -R 777 apply_yamls.sh
./apply_yamls.sh

cd ..
echo "........................................Finished installing PDLC..............................................."
echo ".....................Installing SWM....................................."
cd qos-scheduler
sed -i '59s/enabled: true/enabled: false/' ./helm/qos-scheduler/values.yaml
make chart
helm install qostest --namespace=he-codeco-swm --create-namespace tmp/helm
cd ..
echo "......................................Finished installing SWM.................................."

echo ".....................Installing Kepler....................................."
cd kepler
make build-manifest OPTS="PROMETHEUS_DEPLOY"
kubectl create -f _output/generated-manifest/deployment.yaml
cd ..
echo "......................................Finished installing Kepler.................................."
