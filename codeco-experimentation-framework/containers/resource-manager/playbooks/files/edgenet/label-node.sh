#!/bin/bash

# Copyright (c) 2024 Athena RC.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0
#
# Contributors:
#      Lefteris Mamatas - author
#      George Koukis - contributor

kubectl label nodes kubew1 \
    edge-net.io/as=University_of_Macedonia__Economic_and_Social_Sciences \
    edge-net.io/asn="12364" \
    edge-net.io/city=Thessaloniki \
    edge-net.io/continent=Europe \
    edge-net.io/country-iso=GR \
    edge-net.io/isp=National_Infrastructures_for_Research_and_Technolo \
    edge-net.io/lat=n40.643900 \
    edge-net.io/lon=e22.935800 \
    edge-net.io/state-iso=B
