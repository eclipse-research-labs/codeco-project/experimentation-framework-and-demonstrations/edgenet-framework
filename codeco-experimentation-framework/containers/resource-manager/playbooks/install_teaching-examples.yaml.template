# Copyright (c) 2024 Athena RC.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0
#
# Contributors:
#      Lefteris Mamatas - author
#      George Koukis - contributor

---
- hosts: {@node_name@}
  gather_facts: no
  vars:
    admin_username: {@admin_username@}
    node_name: {@node_name@} 
    node_ip: {@node_ip@}
    app_name: {@app_name@} 
    app_version: {@app_version@}
    #ansible_shell_type: sh
    #ansible_terminal_type: dumb

  tasks:
     - name: upload teaching examples
       become: yes
       become_user: "{{ admin_username }}"
       ansible.builtin.copy:
         src: "{{ item }}"
         dest: "/home/{{ admin_username }}/"
       with_fileglob:
         - "teaching-examples/*"
         - "teaching-examples/linux/*"
         - "teaching-examples/kubernetes/*"
       ignore_errors: true

     - name: convert shell scripts to executable
       become: yes
       become_user: "{{ admin_username }}"
       shell: |
               chmod +x /home/{{ admin_username }}/*.sh
