#!/bin/bash

# Copyright (c) 2024 Athena RC.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0
#
# Contributors:
#      Lefteris Mamatas - author
#      George Koukis - contributor

# import main configuration
source /opt/clusterslice/configuration.sh

# import basic functions
source $main_path/basic_functions.sh

# import input variables
node_name=$NODE_NAME
node_type=$NODE_TYPE

# define only if it is not set
if [[ -z $kubernetes_type ]]; then
   kubernetes_type=$KUBERNETES_TYPE
fi

admin_username=$ADMIN_USERNAME

# import playbook functions
source $main_path/playbook_functions.sh

# wait for cluster only if it is a master node
if [[ $node_type == "mastervm" ]] || [[ $node_type == "masternode" ]]; then
   wait_for_cluster $node_name $admin_username $kubernetes_type
fi
