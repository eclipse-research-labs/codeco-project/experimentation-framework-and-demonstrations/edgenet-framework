#!/bin/bash

# Copyright (c) 2024 Athena RC.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0
#
# Contributors:
#      Lefteris Mamatas - author
#      George Koukis - contributor

# This script counts the number of cluster deployments

# Define the file name
FILE="cluster-deployment"

# Check if the file exists
if [ ! -f "$FILE" ]; then
  # If it doesn't exist, create the file and set initial count to 0
  echo 0 > "$FILE"
fi

# Read the current count from the file
count=$(cat "$FILE")

# Increment the count by 1
count=$((count + 1))

# Save the updated count back to the file
echo "$count" > "$FILE"

# Optionally, print the current count to the console
echo "Deploying cluster number $count."

# Setting appropriate variable
cluster_number=$count
