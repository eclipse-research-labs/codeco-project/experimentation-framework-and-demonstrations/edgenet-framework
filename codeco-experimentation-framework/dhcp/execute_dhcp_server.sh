#!/bin/bash

# Copyright (c) 2024 Athena RC.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0
#
# Contributors:
#      Lefteris Mamatas - author
#      George Koukis - contributor

# import basic functions
source ../basic_functions.sh

is_dhcp_server_image_available=$(is_image_available "swnuom/dhcp-server:latest")
if [[ $is_dhcp_server_image_available == "true" ]]; then
   echo "Docker image swnuom/dhcp-server is available."
else
   echo "Docker image swnuom/dhcp-server is not available."
   echo "Compiling image."
   cd ../containers/dhcp-server
   ./update_image
   cd ../../dhcp/
fi

# remove existing clusterslice-dhcp, in the case it exists
docker stop dhcp-server 2> /dev/null > /dev/null
docker remove dhcp-server 2> /dev/null > /dev/null

docker run -d --name dhcp-server --net=host -v ~/codeco-experiments/dhcp/:/etc/dhcp/ swnuom/dhcp-server
