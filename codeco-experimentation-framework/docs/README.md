# Documentation of the CODECO Experimentation Framework (CODEF)

## Table of Contents

- [Overview](#overview)
- [Architecture](#architecture)
- [Features](#features)
  - [Automated experiments](#automated-experiments)
  - [Multi-Clustering and Multi-Domain Capabilities](#multi-clustering-and-multi-domain-capabilities)
  - [Debugging](#debugging)
- [List of directories and scripts](#list-of-directories-and-scripts)
- [Requirements](#requirements)
- [Quickstart](#quickstart)
- [CODEF components](#codef-components)
  - [Configurations](#configurations)
  - [DHCP server](#dhcp-server)
  - [Infrastructure Managers](#infrastructure-managers)
    - [XCP-ng](#xcp-ng)
    - [VirtualBox](#virtualbox)
    - [AWS](#aws)
    - [CloudLab](#cloudlab)
  - [Resource Managers](#resource-managers)
- [Experiments](#experiments)
  - [Performance of Network Plugins across Kubernetes Flavors](#1-performance-of-network-plugins-across-kubernetes-flavors)
  - [Deployment of Anomaly Detection methods](#2-deployment-of-anomaly-detection-methods)
  - [Deployment of the L2S-M solution](#3-deployment-of-the-l2s-m-solution)
  - [Deployment of a modified CODECO-EdgeNet cluster](#4-deployment-of-a-modified-codeco-edgenet-cluster)
  - [Deployment of CODECO through the ACM component](#5-deployment-of-codeco-through-the-acm-component)
- [Frequent error fixes](#frequent-error-fixes)
- [Contributing](#contributing)
- [License](#license)
- [Acknowledgments](#acknowledgments)
- [Contact](#contact)


## Overview
The **CODECO Experimentation Framework** (also referred to as CODEF for short) is an open-source solution for the rapid experimentation of K8s-based edge cloud deployments. It adopts a microservice-based architecture and introduces innovative abstractions for: 
1. **holistic** deployment of Kubernetes clusters and associated applications, starting from the VM allocation level,
2. **declarative** cross-layer experiment configuration,
3. **automation** features that execute the experiment configuration up to the visualization of produced results. 


## Architecture
It extends the [Clusterslice](https://github.com/SWNRG/clusterslice/tree/main) solution, following a more lightweight approach, as it requires only Docker for execution. It supports additional edge capabilities and environments and experimentation automation features through its innovative abstractions. The framework's architecture includes the following components:
+ **Experiment Manager**: the "boss" which oversees and coordinates all experiment processes, ensuring the reliable execution, reproducibility, automatic deployment and control of experiments.
+ **Infrastructure Manager**: responsible for allocating the cluster nodes over heterogeneous test-beds and cloud systems (i.e. including physical nodes or VMs) and the OS installation.
+ **Resource Managers**: provide a node-level automation abstraction for the software deployment of cluster nodes. They oversee the deployment of user-defined applications through the utilization of [Ansible playbook templates](). After the allocation of cluster nodes from the Infrastructure Manager, a number of Resource Managers are deployed, one for each node.
+ **Experiment Controller**: responsible for the simple and automated experimentation execution and result creation. The user just defines the experiments, the specific metrics and the number of replications.
+ **Results Processor**: evaluates and post-processes the experiment results. Performs statistical evaluations (e.g. mean values), plotting based on the pre-defined metrics, and automating the creation of LaTeX-based report PDF files, incorporating the generated plots.

![CODEF Architecture](CODEF_architecture.png)

## Features
TBA

### Automated experiments
CODEF covers complete experimentation automation, from cluster deployment to experiment execution and results processing.
Refer to the `execute-*` scripts (.sh) in the [examples](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/tree/main/codeco-experimentation-framework/examples?ref_type=heads) folder for the automated experimentation of proof of concept  

### Multi-Clustering and Multi-Domain Capabilities
TBA

+ Integration with external edge environments and testbeds e.g., AWS, EdgeNet (not working currently), Cloudlab (TBA).
+ Utilization of multi-clustering tools like **OCM**, **Liqo** and **Karmada** (already supported as Ansible template playbooks).
+ [Thanos]() playbook TBA for multi-cluster monitoring.

### Debugging
TBA

Refer to the ["Frequent error fixes"](#frequent-error-fixes) section for some usual fixes.

+ The `valid-configurations.json` file includes all the available parameters for the installation scripts. Prints messages in case something is wrong. 
+ The ansible visualizer prints messages per playbook execution after running the installation scripts. Prints exit code in case something is wrong. Stop the execution (Ctrl + C).

## List of directories and scripts

| Directory / Script Name       | Description                                                |
|-------------------|------------------------------------------------------------|
| [`basic_functions.sh`](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/blob/main/codeco-experimentation-framework/basic_functions.sh?ref_type=heads)      |  Script used for troubleshooting and function parsing  |
| [`cluster_deployment.sh`](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/blob/main/codeco-experimentation-framework/cluster_deployment.sh) |   Script used for CODEF functionality, Infrastructure Manager deployment and docker-compose.yaml generation. **Note to Enable/Disable DHCP if needed**   |
| [`create_metrics.sh`](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/blob/main/codeco-experimentation-framework/create_metrics.sh?ref_type=heads) |   Script used for the metrics creation and LateX generation     |
| [`experiments_controller.sh`](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/blob/main/codeco-experimentation-framework/experiments_controller.sh?ref_type=heads) |   Script used for the automated experiments using KNB. Used by the scripts in the [examples](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/blob/main/codeco-experimentation-framework/examples?ref_type=heads) folder  |
| [`get_results_net.sh`](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/blob/main/codeco-experimentation-framework/get_results_net.sh?ref_type=heads) |   Script used to calculate and display average values of the KNB experiment   |
| [`import_configuration.sh`](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/blob/main/codeco-experimentation-framework/import_configuration.sh?ref_type=heads) |   Script used for troubleshooting i.e. if a specific configuration file exists and CODEF installation to different infrastructures  |
| [`ssh_without_password.sh`](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/blob/main/codeco-experimentation-framework/ssh_without_password.sh?ref_type=heads) |   Script used for password-less ssh access to the nodes. Node is added to the _authorized_keys_ directory   |
| [`valid-configurations.json`](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/blob/main/codeco-experimentation-framework/valid_configurations.json?ref_type=heads) |   Json file used to ensure that the parameters given during the installation process match the expected values for successful deployment. **Note that this needs to be filled by the user**   |
| [`validate_input.sh`](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/blob/main/codeco-experimentation-framework/validate_input.sh?ref_type=heads) |   Script used for troubleshooting and validation of inputs when running installation scripts   |

| Directory       | Description                                                |
|-------------------|------------------------------------------------------------|
| [`benchmarks/`](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/blob/main/codeco-experimentation-framework/benchmarks?ref_type=heads) |   Includes the [k8s-bench-suite](https://github.com/InfraBuilder/k8s-bench-suite/tree/master) benchmarking tool (KNB) |
| [`configurations/`](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/tree/main/codeco-experimentation-framework/configurations?ref_type=heads) |   Includes the configuration files used to define the cluster to be deployed i.e. master(s)/worker(s)' name, IP, MAC, the Infrastructure Manager, and credentials  |
| [`containers/`](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/tree/main/codeco-experimentation-framework/containers?ref_type=heads) |   Includes the Infrastructure Managers, DHCP server and Resource Manager   |
| [`docs/`](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/tree/main/codeco-experimentation-framework/docs?ref_type=heads) |   Includes detailed information and documentation about CODEF   |
| [`examples/`](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/tree/main/codeco-experimentation-framework/examples?ref_type=heads) |   Includes some example installation scripts e.g., ACM, L2S-M and scripts for automated experimentation about CNI plugins and K8s distributions using KNB   |
| [`results/`](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/blob/main/codeco-experimentation-framework/results?ref_type=heads) |   The destination of the results of the automated experiments located in the [examples/](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/tree/main/codeco-experimentation-framework/examples?ref_type=heads) folder  |

## Requirements
You should have the following prerequisites to use the CODEF:
1.	Install `Docker`. If `Docker` is not installed follow the [installation instructions](https://docs.docker.com/engine/install/).
2.	Install `jq`. If `jq` is not installed follow the [installation instructions](https://jqlang.github.io/jq/download/)
3.	Have **_password-less ssh access_** to the nodes you want to deploy. For instance, use the `[ssh_without_password.sh](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/blob/main/codeco-experimentation-framework/ssh_without_password.sh?ref_type=heads)` script to add the nodes to the _authorized_keys_ directory.


## Quickstart
1. **Clone** the repository to your local workshop e.g. node/VM that will manage the deployment of nodes, clusters and applications across your infrastructure e.g., in this case a VM with the name "boss":

```console
user@boss:~$ git clone https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework.git
user@boss:~$ cd codeco-experimentation-framework/
```

2. Ensure the necessary [**requirements**](#requirements) are met.

3. **Create** your configuration file that includes the information of your nodes (take as a template the [example-configuration](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/blob/main/codeco-experimentation-framework/configurations/example-configuration?ref_type=heads) file and **modify** it with your preferred editor):

```console
user@boss:~/codeco-experimentation-framework/configurations$ nano athena-configuration
```

4. Make sure you have updated the [valid-configurations.json](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/blob/main/codeco-experimentation-framework/valid-configurations.json?ref_type=heads) with the necessary information. This file will be used for validation of the inputs when running an installation script.

5. **Modify** and **run** an example installation script. For instance the [install-cluster.sh](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/blob/main/codeco-experimentation-framework/examples/install-cluster.sh?ref_type=heads) script:

```console
user@boss:~/codeco-experimentation-framework/examples$ nano install-cluster.sh
user@boss:~/codeco-experimentation-framework/examples$ ./install-cluster.sh
```

[![asciicast](https://asciinema.org/a/mpJOdm1pNq2rZmoO9e3GKa3ay.svg)](https://asciinema.org/a/mpJOdm1pNq2rZmoO9e3GKa3ay)


## CODEF components

**Note: Remember to use the _./update_image_ script located in each `container/` subfolder in case you modified the scripts or created new ones.** 


### Configurations
In the [configurations folder](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/tree/main/codeco-experimentation-framework/configurations?ref_type=heads) create a configuration file that includes the information for the cluster deployment. Copy and modify the [example-configuration](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/blob/main/codeco-experimentation-framework/configurations/example-configuration?ref_type=heads) file as seen below. This configuration file will be used as a parameter inside the installation scripts (e.g., using the [install_cluster.sh](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/blob/main/codeco-experimentation-framework/examples/install-cluster.sh?ref_type=heads) for the deployment of the expected nodes in the selected infrastructure.
```console
#!/bin/bash

# generic configuration parameters - only if they are not already set
: ${admin_username:=athena}                      # define admin username
: ${admin_password:=ENCODED}                     # define sha-512 encoded password
: ${cloud_ip=10.0.1.1}                           # define cloud server IP
: ${cloud_name=my_cloud}                         # define name of cloud server
: ${cloud_operator:=infrastructure-manager-aws}  # define infrastructure manager to use
: ${kubernetes_type=vanilla}                     # K8s edge distribution
: ${kubernetes_networkfabric=flannel}            # CNI plugin
: ${node_osimage=ubuntu-22-clean}                # define VM image name
: ${master_hosts='["hostm1"]'}
: ${master_ips='["10.0.0.1"]'}
: ${master_macs='["86:16:91:ec:09:01"]'}
: ${worker_hosts='["hostw1","hostw2"]'}
: ${worker_ips='["10.0.0.2","10.0.0.3"]'}
: ${worker_macs='["86:16:91:ec:09:05","86:16:91:ec:09:06"]'}
: ${app_names='["benchmarks"]'}                  # define application(s)
: ${app_scopes='["cluster"]'}                    # define scope of application(s)
```

### DHCP server
In case a DHCP server is needed:
+ Copy the [dhcpd.conf.example](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/blob/main/codeco-experimentation-framework/containers/dhcp-server/dhcpd.conf.example?ref_type=heads) and create a `dhcpd.conf` file.
+ Modify the `dhcpd.conf` to include the information of the nodes to be deployed.
+ Keep the "check_dhcp=true" in the [cluster_deployment.sh](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/blob/main/codeco-experimentation-framework/cluster_deployment.sh?ref_type=heads).


### Infrastructure Managers
The Infrastructure Manager is a technology-agnostic abstraction over heterogeneous test-beds and cloud systems, which is responsible for allocating the cluster nodes from physical node or VM allocation to OS installation phases. Specifications related to the allocation of nodes such as the number of master and worker nodes, OS image and snapshot usage are the inputs of Infrastructure Manager which in turn returns the addresses of allocated physical nodes or VMs. 

**Note: Ensure that you have passwordless _SSH_ access to the nodes you want to deploy or to the cloud server you are using (e.g. in case you want to use CODEF to deploy clusters into your server).**

**Note: The different Infrastructure Managers are located in the [containers/](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/blob/main/codeco-experimentation-framework/containers?ref_type=heads) folder of the CODEF. For each one of the supported cloud servers, you should **create a configuration file** in the form of `configuration.server_ip`.**

_Table: List of available Infrastructure Managers_

| Infrastructure Manager       | Description                                     | Status |
|-------------------|------------------------------------------------------------|---------|
| **XCP-ng**     |  [Xen Cloud Platform - Next Generation (XCP-ng)](https://xcp-ng.org/) is a virtualization platform and an open-source hypervisor based on the Xen hypervisor. It was created as a free and open-source alternative to Citrix Hypervisor (formerly XenServer)  | Supported |
| **VirtualBox**      |  [VirtualBox](https://www.virtualbox.org/) is a cross-platform, open-source hypervisor developed by Oracle that allows users to create and run virtual machines (VMs) on a host operating system  | Supported |
| **AWS**      |  [Amazon Web Services (AWS)](https://aws.amazon.com/) is a comprehensive and widely adopted cloud computing platform provided by Amazon. It offers a vast array of services, including computing power, storage options, networking, databases, machine learning, analytics, and more.   | Supported |
| **CloudLab**   |  [CloudLab](https://www.cloudlab.us/) is an open scientific infrastructure/testbed designed to support research in cloud computing, distributed systems, and networking. It provides researchers with access to large-scale, customizable cloud environments where they can deploy and experiment with novel cloud architectures and applications  | TBA |


### XCP-ng
For environments using the XCP-ng hypervisor, CODEF supports the [XCP-ng Infrastructure Manager](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/tree/main/codeco-experimentation-framework/containers/infrastructure-manager-xcpng?ref_type=heads).

**Instructions:**
1. Find the [configuration.ip](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/blob/main/codeco-experimentation-framework/containers/infrastructure-manager-xcpng/configuration.ip?ref_type=heads) file (see below),
2. Copy it,
3. Rename it by replacing the "ip" with the IP of your server e.g., _configuration.10.0.0.1_ and modify it.
4. After that, the `configuration files` that include the argument _: ${cloud_operator:=infrastructure-manager-aws}_ will be using the XCP-ng Infrastructure Manager and deploy the defined nodes to your infrastructure.
5. Modify and run the _install-cluster.sh_ script with the created configuration file.
6. SSH access to the cluster after the script is done.
```console
#!/bin/bash

# basic xcp-ng configuration
sr_uuid="TBA"
private_net_uuid="TBA" # private network
public_net_uuid="TBA" # public network
host_uuid="TBA" # server uuid

# configuration parameters
# enable if virtualization infrastructure supports server pooling
is_pooling_enabled=true

# remove resource, if one with the same name already exists
force_resource_removal=true

# enable snapshot, e.g., revert to a snapshot if the VM+snapshot already exist
enable_snapshots=true

# create alias for remote xe CLI execution, i.e., to be executed via ssh
xe="ssh root@$server -o StrictHostKeyChecking=no xe"
# local execution
#xe="xe"
```

Some **usual commands** to get the information needed for your XCP-ng server are listed below:
+ _Outputs a list of storage repositories along with their UUIDs and types. Look for the storage repository you want and note the UUID._
```console
xe sr-list 
```

+ _Displays all the available networks, including their UUIDs and associated interfaces. You'll need to identify the public and private networks based on their names or descriptions and note the corresponding UUIDs._
```console
xe network-list 
```

+ _Shows the UUID and name-label of the host. Note the host UUID from the output._
```console
xe host-list 
```

**Note: The current _node_osiimage_ used as "VM template" for the creation of the VMs is located in _/root/backup/_ directory of the XCP-ng server and is defined in [create_vm.sh]() with the name _ubuntu-22-clean.xva_**


### VirtualBox
**TBT (To Be Tested further)**

For local machines using VirtualBox, CODEF supports the [VirtualBox Infrastructure Manager](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/blob/main/codeco-experimentation-framework/containers/infrastructure-manager-virtualbox?ref_type=heads).

**Instructions:**
1. Find the [configuration.ip](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/blob/main/codeco-experimentation-framework/containers/infrastructure-manager-virtualbox/configuration.ip?ref_type=heads) file (see below),
2. Copy it,
3. Rename it by replacing the "ip" with the IP of your VM e.g., _configuration.10.0.0.1_ and modify it.
4. After that, the `configuration files` that include the argument _: ${cloud_operator:=infrastructure-manager-virtualbox}_ will be using the VirtualBox Infrastructure Manager and deploy the defined nodes to your computer.
5. Modify and run the _install-cluster.sh_ script with the created configuration file.
6. SSH access / Open the VMs after the script is done.
```console
#!/bin/bash

# basic virtualbox configuration
image_path="TBA"
username="TBA"

# configuration parameters
# enable if virtualization infrastructure supports server pooling
is_pooling_enabled=false

# remove resource, if one with the same name already exists
force_resource_removal=true

# enable snapshot, e.g., revert to a snapshot if the VM+snapshot already exist
enable_snapshots=true

# create alias for remote xe CLI execution, i.e., to be executed via ssh
VBoxManage="ssh $username@$server -o StrictHostKeyChecking=no VBoxManage"
# local execution
#VBoxManage="VBoxManage"
```

**Note: The current _node_osiimage_ used as "VM template" for the creation of the VMs is located in the defined _image_path/_ directory of the user's computer and is defined in [create_vm.sh]()**


### AWS
CODEF also supports an Infrastructure Manager for the Amazon Web Services (AWS) shared cloud space of the CODECO Project - provided and maintained by IBM. Currently, only registered and authorized project partners have access to the shared cloud space through VPN ([Tailscale](https://tailscale.com/)).

**Instructions:**
Authorized partners should:
1. Login to the Tailscale VPN
2. Find the [configuration.<ip>]() file. This only exists in the private repo.
3. Modify the [aws-experimentation-cluster]() configuration file to use the desired nodes to be used. This only exists in the private repo.
4. Modify an installation script e.g., [install-cluster.sh](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/blob/main/codeco-experimentation-framework/examples/install-cluster.sh?ref_type=heads) to use the _configuration_file=configurations/aws-experimentation-cluster_ argument
5. **Please ensure the desired nodes/cluster are not used by another partner as by running this script will result in a clean installation and revert the nodes to a clean snapshot!**
6. Run the _install-cluster.sh_ script.
7. SSH access to the cluster after the script is done.

**Note: Due to privacy concerns, the _aws-experimentation-cluster_ configuration file and the _configuration.ip_ are currently uploaded only to the CODECO private repo.**


### CloudLab
TBA


## Resource Managers
The Resource Managers provide a node-level automation abstraction for the software deployment of cluster nodes. They oversee the deployment of user-defined applications, supporting a range of them through the utilization of Ansible playbook templates  while also determining their versioning and deployment scope. 
The set of the custom Ansible playbooks templates used for the automated deployment of the various **applications** can be found in the [playbooks folder](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/tree/main/codeco-experimentation-framework/containers/resource-manager/playbooks?ref_type=heads) of the Resource Manager. These applications/software can be easily installed in the cluster to be deployed by just writing their name (same as the `install_<app_names>.yaml.template` is defined in the `playbooks/` folder) into the `app_names` field, the available version (if different versions are defined in the playbooks) into the `app_version` field, and the scope  of their installation into the `app_scopes` e.g., "cluster", "masters", "workers", "all". CODEF currently supports:

| **Application / Software**  | **Installation Script Parameters**                          | **Category**                    | **Description**                                                           |
|-----------------------------|------------------------------------------------------------|----------------------------------|---------------------------------------------------------------------------|
| **Kubernetes**              | `kubernetes_type`, `kubernetes_networkfabric`, `kubernetes_networkfabricparameters` | Orchestration Software           | Configures master and worker node variants for Kubernetes clusters.        |
| **Docker**                  | `app_names`                                                | Containerization Platform        | Installs and manages the Docker Engine for containerized applications.     |
| **Argo**                    | `app_names`                                                | Workflow Orchestration           | Manages Argo Workflows for Kubernetes-native workflows automation.         |
| **Helm**                    | `app_names`                                                | Package Manager                  | Supports Helm charts for simplified Kubernetes application deployments.    |
| **Kubernetes Dashboard**    | `app_names`                                                | GUI Tool                         | Provides a web-based interface to manage Kubernetes resources.             |
| **Kubeview**                | `app_names`                                                | Visualization Tool               | Offers a GUI to visualize Kubernetes clusters and node structure.          |
| **Metrics Server**          | `app_names`                                                | Monitoring Tool                  | Kubernetes add-on for resource metrics collection across clusters.         |
| **Prometheus-Grafana-Alertmanager**| `app_names`                                                | Monitoring & Alerting Tool       | Deploys the Prometheus stack with Grafana for monitoring and alerting.     |
| **Benchmarks**              | `app_names`                                                | Benchmarking Tool                | Kubernetes Benchmarking tools e.g., [k8s-bench-suite](https://github.com/InfraBuilder/k8s-bench-suite)                                    |
| **Kube-burner**             | `app_names`                                                | Benchmarking Tool                | Executes performance tests and benchmarking on Kubernetes environments.    |
| **OCM**                     | `app_names`                                                | Multi-cluster Management         | Facilitates management of multiple Kubernetes clusters with OCM.           |
| **Liqo**                    | `app_names`                                                | Multi-cluster Management         | Implements multi-cluster management via the Liqo solution.                 |
| **Karmada**                 | `app_names`                                                | Multi-cluster Management         | Karmada enables unified control plane across multiple Kubernetes clusters. |
| **Submariner**              | `app_names`                                                | Networking Plugin Solution       | Provides inter-domain network connectivity for Kubernetes clusters.        |
| **NetMA L2S-M**             | `app_names`                                                | Networking Solution              | Deploys the [L2S-M CODECO NetMA](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/network-management-and-adaptation-netma) solution for advanced network management.   |
| **CODECO ACM**              | `app_names`                                                | Cluster Management Component     | [ACM CODECO component](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/acm) for installing all CODECO components (SWM, MDM, PDLC & NetMA) into the clusters. The main entry point to the CODECO platform.  |


**Note: You can also review all the available parameters in the [`valid-configurations.json`](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/blob/main/codeco-experimentation-framework/valid-configurations.json?ref_type=heads) file.**

**Note: Remember to use the _./update_image_ script located in the `playbook/` subfolder in case you modified the scripts or created new ones and update the `valid-configurations.json` file.** 


## Experiments
Some indicative experiments done utilizing the CODEF includes:
### 1. **Performance of Network Plugins across Kubernetes Flavors** 

This experiment covers the performance evaluation of different CNI plugins (and the L2S-M solution) across various K8s distributions. Currently, the following CNI plugins are supported for each considered Kubernetes flavor:
  + vanilla: Flannel, Multus, Calico, WeaveNet, Cilium, Kube-Router, Kube-OVN, Antrea
  + k3s: Flannel, Calico, Cilium
  + k0s: Kube-Router, Calico
  + microk8s: Calico, Flannel, Kube-OVN

_Cited works: Koukis et al., INFOCOM & ISCC 2024 [[3](#ref3),[4](#ref4)]_

![Knb experiment](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/blob/main/codeco-experimentation-framework/docs/knb_experiment.png?ref_type=heads)

**Teaser Video:** evaluation of various **network plugins** (e.g. flannel, calico etc.) across **different Kubernetes distributions** (e.g. vanilla K8s, K3s, K0s, MicroK8s) in terms of CPU, RAM and throughput. 
[![asciicast](https://asciinema.org/a/apmBQn5EJzRmnYyjYoKnWbQ0l.svg)](https://asciinema.org/a/apmBQn5EJzRmnYyjYoKnWbQ0l)

### 2. **Deployment of Anomaly Detection methods** 

In this case, Change Point-based algorithms in Edge-Cloud systems are deployed utilizing Argo Workflows and containerized MATLAB functions for the deployment of the CP mechanisms. Some relevant metrics include the:
+ actual detection delay (i.e. experimentally measured),
+ detection delay in data instances (i.e. theoretical),
+ median & average response time,
+ resource utilization (CPU and memory consumption)

_Cited work: Skaperas et al., INFOCOM 2024 [[2]](#ref2)_

**Teaser Video:** the deployment of **Anomaly Detection workflows** 
[![asciicast](https://asciinema.org/a/dGUD4EbJwtE77VKrHHGW7tuQm.svg)](https://asciinema.org/a/dGUD4EbJwtE77VKrHHGW7tuQm)

### 3. **Deployment of the L2S-M solution**

A Kubernetes networking solution, included in the CODECO NetMA component, that enables virtual networking in Kubernetes clusters. Please refer to the [official L2S-M repository](https://github.com/Networks-it-uc3m/L2S-M/tree/main) for more information.
We support both the v1 and v2 of the solution. Just write "l2s-m" in the _app_names='[""]'_ field and the version ("v1" or "v2") in the _app_versions='[""]'_ of the [_install-l2sm-cluster.sh_](), run the script and a cluster with the L2S-M installed in all nodes according to the playbook script should be deployed.
```console
user@boss:~/codeco-experimentation-framework/examples$ ./install-l2sm-cluster.sh
```

### 4. **Deployment of a modified CODECO-EdgeNet cluster**

Just run the [_install-edgent-cluster.sh_]() script and a CODECO-EdgeNet cluster with the EdgeNet features installed should be deployed.
```console
user@boss:~/codeco-experimentation-framework/examples$ ./install-edgenet-cluster.sh
```
For a detailed description of the features, installation and demos of EdgeNet please refer to the public [CODECO repository](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/edgenet-framework/-/tree/main/installation?ref_type=heads) and watch the illustrated demos for the installation and utilization of the EdgeNet features/CRDs within this local Kubernetes cluster environment, in particular the:
+ _Multiprovider_ (i.e. contribute a node into an EdgeNet cluster) feature and
+ _Multitenancy_ (i.e. enabling the utilization of a shared cluster such as tenant and role requests, slice, subnamespaces etc.) feature.

**Teaser Video:** the deployment of a **local CODECO-EdgeNet cluster** and its features.
[![asciicast](https://asciinema.org/a/6DvIJvtEq2AEJ6o4YJoz7jTIh.svg)](https://asciinema.org/a/6DvIJvtEq2AEJ6o4YJoz7jTIh)

**Note: Please note that the deployment of EdgeNet software depends on the status of the official [EdgeNet repository](https://github.com/EdgeNet-project/edgenet) and _is currently not working_ as Google removed K8s v1.23 from the repositories.**

### 5. **Deployment of CODECO through the ACM component**

The ACM component will act as the entry point to the CODECO platform and manage the installation of the other CODECO components. For further details about ACM please refer to the [ACM subgroup](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/acm). Just run the [_install-acm-cluster.sh_]() and a cluster with an ACM version should be deployed.
```console
user@boss:~/codeco-experimentation-framework/examples$ ./install-acm-cluster.sh
``` 


## Frequent error fixes
- **Docker should be running**. Specifically, if you running CODEF from your WSL2 (Windows OS) ensure docker_engine is running and WLS integration is enabled for your distros e.g. Ubuntu
- **When running an installation script, the valid_input script prints debugging messages if something is wrong. Add your configuration's and cluster's information in the valid-configurations.json file**
- **In case you run CODEF from a machine in different networks than your server please change the "check_dhcp" parameter inside the "cluster_deployment.sh" script to "false" i.e. "check_dhcp=false"**


## Contributing
TBA

## License
TBA

## Acknowledgements
Please **cite** this paper, if you use CODEF:

1. <a id="ref1"></a>G. Koukis, S. Skaperas, I. A. Kapetanidou, V. Tsaoussidis, L. Mamatas, "An Open-Source Experimentation Framework for the Edge Cloud Continuum",  INFOCOM 2024 - CNERT: 11th International Workshop on Computer and Networking Experimental Research using Testbeds [link](https://ieeexplore.ieee.org/document/10620846)

Additional papers using CODEF. These can be also found in [CODECO Zenodo](https://zenodo.org/communities/he-codeco/)

2. <a id="ref2"></a>S. Skaperas, G. Koukis, I. A. Kapetanidou, V. Tsaoussidis and L. Mamatas,   "A Pragmatical Approach to Anomaly Detection Evaluation in Edge Cloud Systems", INFOCOM 2024 - ICCN: 6th IEEE International Workshop on Intelligent Cloud Computing and Networking [link](https://ieeexplore.ieee.org/document/10620733)

3. <a id="ref3"></a>G. Koukis, S. Skaperas, I. A. Kapetanidou, L. Mamatas, V. Tsaoussidis, "Performance Evaluation of Kubernetes Networking Approaches across Constraint Edge Environments", ISCC 2024 - NGMSE: Next-Generation Multimedia Services at the Edge: Leveraging 5G and Beyond [link](https://ieeexplore.ieee.org/document/10733726)

4. <a id="ref4"></a>G. Koukis, S. Skaperas, I. A. Kapetanidou, L. Mamatas, V. Tsaoussidis, "Evaluating CNI Plugins Features & Tradeoffs for Edge Cloud Applications", ISCC 2024 - ECO: 1st International Workshop on Edge to Cloud Orchestration [link](https://ieeexplore.ieee.org/document/10733657)

Clusterslice paper that influenced CODEF:

5. <a id="ref5"></a>L. Mamatas, S. Skaperas, and I. Sakellariou, "ClusterSlice: Slicing Resources for Zero-touch Kubernetes-based Experimentation", Technical Report, University of Macedonia, https://ruomo.lib.uom.gr/handle/7000/1757, November 2023.


## Contact
Contact [Lefteris Mamatas](https://sites.google.com/site/emamatas/) from the University of Macedonia, Greece.
