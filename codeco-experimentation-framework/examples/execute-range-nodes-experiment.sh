#!/bin/bash

# Copyright (c) 2024 Athena RC.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0
#
# Contributors:
#      Lefteris Mamatas - author
#      George Koukis - contributor

# name of experiment
experiment_name="range-nodes-experiment"

# variables to range in different runs
ranged_variables='[{"run_id":"k8s-3-nodes","variables":{"kubernetes_type":"vanilla","kubernetes_networkfabric":"flannel","worker_hosts":["workername1","workername2"],"worker_ips":["workerip1","workerip2"], "worker_macs":["workermac1","workermac2"]}}'

# set the number of replications
replications_number=1

# basic cluster configuration options
#configuration_file='configurations/aws-experimentation-cluster'
configuration_file='configurations/example-configuration'

# define benchmariking applications to run
app_names='["benchmarks"]'
app_scopes='["cluster"]'

# benchmarking command
# CHANGE the <worker1,2> with the actual node names
benchmarking_command="knb -cn <worker1> -sn <worker2> -d 10 -t 60 -o json" 

# replication rest time
replication_rest_time=300 # 300

# experiment run rest time (i.e., per cluster deployment)
run_rest_time=10

# format of experiment output (e.g., json)
output_format='json'

# structure of experiment output
metrics='{
  "net": {
    "values": [
      ".data.idle.bandwidth",
      ".data.pod2pod.tcp.bandwidth",
      ".data.pod2pod.udp.bandwidth",
      ".data.pod2svc.tcp.bandwidth",
      ".data.pod2svc.udp.bandwidth"
    ],
    "columns": [
      "plugin",
      "idle",
      "pod2pod-tcp",
      "pod2pod-udp",
      "pod2svc-tcp",
      "pod2svc-udp"
    ],
    "rows": "k8s internal network throughput (Mbps)"
  },
  "cpu": {
    "values": [
      ".data.idle.client.cpu.total",
      ".data.idle.server.cpu.total",
      ".data.pod2pod.tcp.client.cpu.total",
      ".data.pod2pod.tcp.server.cpu.total",
      ".data.pod2pod.udp.client.cpu.total",
      ".data.pod2pod.udp.server.cpu.total",
      ".data.pod2svc.tcp.client.cpu.total",
      ".data.pod2svc.tcp.server.cpu.total",
      ".data.pod2svc.udp.client.cpu.total",
      ".data.pod2svc.udp.server.cpu.total"
    ],
    "columns": [
      "plugin",
      "idle-client",
      "idle-server",
      "pod2pod-tcp-client",
      "pod2pod-tcp-server",
      "pod2pod-udp-client",
      "pod2pod-udp-server",
      "pod2svc-tcp-client",
      "pod2svc-tcp-server",
      "pod2svc-udp-client",
      "pod2svc-udp-server"
    ],
    "rows": "CPU usage (%)"
  },
  "mem": {
    "values": [
      ".data.idle.client.ram",
      ".data.idle.server.ram",
      ".data.pod2pod.tcp.client.ram",
      ".data.pod2pod.tcp.server.ram",
      ".data.pod2pod.udp.client.ram",
      ".data.pod2pod.udp.server.ram",
      ".data.pod2svc.tcp.client.ram",
      ".data.pod2svc.tcp.server.ram",
      ".data.pod2svc.udp.client.ram",
      ".data.pod2svc.udp.server.ram"
    ],
    "columns": [
      "plugin",
      "idle-client",
      "idle-server",
      "pod2pod-tcp-client",
      "pod2pod-tcp-server",
      "pod2pod-udp-client",
      "pod2pod-udp-server",
      "pod2svc-tcp-client",
      "pod2svc-tcp-server",
      "pod2svc-udp-client",
      "pod2svc-udp-server"
    ],
    "rows": "RAM usage (MB)"
  }
}'

graphs='[
    {
        "name": "k8s-distros-throughput",
        "filename": "net.csv",
        "title": "Internal Throughput of K8s Distributions",
        "striptitle": "yes",
        "transpose": "yes",
        "filterkeyword": "no",
        "removekeyword": "no",
        "xlabel": "Communication Type",
        "ylabel": "Throughput(Mbps)",
        "xrange": "auto",
        "yrange": "[0:5000]",
        "boxvertical": "top",
        "boxhorizontal": "right",
        "xticksrotate": "-45 scale 0"
    },
    {
        "name": "k8s-distros-cpu-client",
        "filename": "cpu.csv",
        "title": "CPU Utilization of K8s Distributions (Client)",
        "striptitle": "yes",
        "transpose": "yes",
        "filterkeyword": "server",
        "removekeyword": "-client",
        "xlabel": "Communication Type",
        "ylabel": "CPU(%)",
        "xrange": "auto",
        "yrange": "[0:40]",
        "boxvertical": "top",
        "boxhorizontal": "left",
        "xticksrotate": "-45 scale 0"
    },
    {
        "name": "k8s-distros-mem-client",
        "filename": "mem.csv",
        "title": "RAM Consumption of K8s Distributions (Client)",
        "striptitle": "yes",
        "transpose": "yes",
        "filterkeyword": "server",
        "removekeyword": "-client",
        "xlabel": "Communication Type",
        "ylabel": "RAM(MB)",
        "xrange": "auto",
        "yrange": "[0:900]",
        "boxvertical": "bottom",
        "boxhorizontal": "right",
        "xticksrotate": "-45 scale 0"
    },
    {
        "name": "k8s-distros-cpu-server",
        "filename": "cpu.csv",
        "title": "CPU Utilization of K8s Distributions (Server)",
        "striptitle": "yes",
        "transpose": "yes",
        "filterkeyword": "client",
        "removekeyword": "-server",
        "xlabel": "Communication Type",
        "ylabel": "CPU(%)",
        "xrange": "auto",
        "yrange": "[0:40]",
        "boxvertical": "top",
        "boxhorizontal": "left",
        "xticksrotate": "-45 scale 0"
    },
    {
        "name": "k8s-distros-mem-server",
        "filename": "mem.csv",
        "title": "RAM Consumption of K8s Distributions (Server)",
        "striptitle": "yes",
        "transpose": "yes",
        "filterkeyword": "client",
        "removekeyword": "-server",
        "xlabel": "Communication Type",
        "ylabel": "RAM(MB)",
        "xrange": "auto",
        "yrange": "[0:900]",
        "boxvertical": "bottom",
        "boxhorizontal": "right",
        "xticksrotate": "-45 scale 0"
    }
]'

# move to main directory
cd ..

# execute experiment
source experiments_controller.sh
