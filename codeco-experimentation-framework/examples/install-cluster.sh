#!/bin/bash

# Copyright (c) 2024 Athena RC.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0
#
# Contributors:
#      Lefteris Mamatas - author
#      George Koukis - contributor

# import basic cluster configuration options
#configuration_file='configurations/aws-experimentation-cluster'
configuration_file='configurations/example-configuration'

# kubernetes related configuration parameters
kubernetes_type=vanilla 
kubernetes_networkfabric=flannel
#kubernetes_networkfabricparameters="{'param1':'value1','param2':'value2'}"

# application configuration
app_names='["benchmarks"]'
#app_versions='["v2"]'
app_scopes='["cluster"]'

# move to main directory
cd ..

# import configuration
source import_configuration.sh

# deploy cluster
source cluster_deployment.sh
