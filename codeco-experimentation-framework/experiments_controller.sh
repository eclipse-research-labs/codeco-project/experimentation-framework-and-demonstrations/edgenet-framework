#!/bin/bash

# Copyright (c) 2024 Athena RC.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0
#
# Contributors:
#      Lefteris Mamatas - author
#      George Koukis - contributor

# expect experiment input data
# experiment_name: name of experiment, i.e., determines results directory.
# ranged_variables: deployment configuration to range in each experimental run
# replications_number: number of experiment replications for statistical accuracy purposes
# configuration_file: configuration of infrastructure
# app_names & app_scopes: benchmarking applications to run
# benchmarking_command: benchmarking command to execute in each replicated experiment
# replication_rest_time: warm-up period before each replication
# run_rest_time: warm-up period before each deployment
# output_format: format of experiment output, i.e., currently only json is supported
# metrics: definition of metrics configuration in experiment
# graphs: definition of graphs configuration in experiment

# Function to handle the SIGINT signal (Ctrl+C)
cleanup() {
    echo ""
    echo "Caught SIGINT signal! Cleaning up..."
    # Optionally, add commands to stop Docker Compose gracefully
    exit 0
}

# Trap the SIGINT signal
trap cleanup SIGINT

# import configuration
source import_configuration.sh

echo ""
echo "Checking if output-results image is available locally."
is_output_results_image_available=$(is_image_available "swnuom/output-results:latest")
if [[ ${is_output_results_image_available} == "true" ]]; then
   echo "Docker image swnuom/output-results is available."
else
   echo "Docker image swnuom/output-results is not available."
   echo "Compiling image."
   cd containers/output-results
   ./update_image
   cd ../../
fi
echo ""

# abort execution if no benchmarking command has been passed
if [[ -z $benchmarking_command ]]; then
   echo ""
   echo "No benchmarking command has been passed, aborting execution."
   echo ""
   exit 1
fi

# check if experiment_name is being set, if not use an empty string
if [[ -z $experiment_name ]]; then
   # skip using subdirectory for results
   experiment_name=""
else
   # create subdirectory, if it does not exist
   mkdir results/${experiment_name} 2> /dev/null
   # add .gitignore file
   cp results/.gitignore results/${experiment_name}/
fi

# determine name of first master host
master_name=$(json_array_item "$master_hosts" 0)

# keep status if particular results exist
check=false  

# extract run ids
run_ids=$(echo "$ranged_variables" | jq -c '.[]' | jq -r '.run_id')

# iterate through all run_ids
for run_id in $run_ids; do
  # extract parameters for this run_id
  run_id_parameters=$(echo "$ranged_variables" | jq -c --arg run_id "$run_id" '.[] | select(.run_id == $run_id) | .variables')

  # skip run if results already exist (to avoid unneeded cluster deployments)
  echo "check if results exist for $run_id with $replications_number replications"
  check=$(check_if_results_exist $run_id $replications_number $experiment_name)
  if [[ $check == false ]]; then
     # apply experiment with particular cluster deployment"
     echo ""
     echo "***********************************************"
     echo "apply experiment with run_id $run_id"
     echo "***********************************************"

     echo "Preparing cluster"

     # configure particular deployment
     # iterate through each variable name and extract the value
 
     # store key-value pairs in a temporary file
     temp_file=$(mktemp)
     echo "$run_id_parameters" | jq -r 'to_entries[] | "\(.key)=\(.value)"' > "$temp_file"

     # read from the temporary file to avoid a subshell issue
     while IFS='=' read -r key value; do
       # export parameter value
       export $key=$value
       echo "configuring parameter $key as $value."
     done < "$temp_file"

     # Remove the temporary file
     rm "$temp_file"

     # validate parameters again (because they are changed)
     validate_parameters

     # deploy cluster
     source ./cluster_deployment.sh

     # loop according to the replication number
     for ((k=1; k<=$replications_number; k++))
     do
       echo ""
       echo "************************************"
       echo "executing experiment replication $k"
       echo "************************************"

       if [ ! -f results/${experiment_name}/${run_id}-results-$k.txt ]; then
          echo "wait $replication_rest_time secs for nodes to settle down"
          sleep $replication_rest_time
          
	  echo "executing benchmarks"
	  # it seems aws requires double t parameter, need to test it again
	  # in other infrastructures
          echo "ssh -tt $admin_username@$master_name \"export KUBECONFIG=/home/$admin_username/.kube/config; export PATH=$PATH:/opt/clusterslice/bin/; export TERM=xterm; /home/$admin_username/$benchmarking_command\""
	  ssh -tt $admin_username@$master_name "export KUBECONFIG=/home/$admin_username/.kube/config; export PATH=$PATH:/opt/clusterslice/bin/; export TERM=xterm; /home/$admin_username/$benchmarking_command" > results/${experiment_name}/${run_id}-results-$k.txt < /dev/null
       else
          echo "results ${run_id}-results-$k.txt already exist."
       fi
     done
     echo "wait $run_rest_time secs"
     sleep $run_rest_time
  else
     echo "results for experiment run ${run_id} already exist."
  fi
done 

# prepare results
if [[ -z $metrics ]]; then
   echo "No metrics parameter passed, skipping preparing results stage."
else
   if [[ $output_format != "json" ]] && [[ $output_format != "JSON" ]]; then
      echo "Only json output format is supported at this stage."
      exit 1
   fi
   echo ""
   echo "Preparing results."
   echo ""
   source ./get_results_net.sh
fi

# create appropriate metrics file
if [[ -z $graphs ]]; then
   echo "No graphs parameter passed, skipping creating metrics and output pdf stages."
else 
   echo ""
   echo "Creating appropriate metrics file"
   echo ""
   source ./create_metrics.sh

   # create output PDF
   echo ""
   echo "Creating output PDF file"
   echo ""
   source ./output_results_container.sh
fi
