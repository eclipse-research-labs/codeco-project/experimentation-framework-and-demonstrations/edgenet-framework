#!/bin/bash

# Copyright (c) 2024 Athena RC.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0
#
# Contributors:
#      Lefteris Mamatas - author
#      George Koukis - contributor

# Check if the configuration file exists
if [ -e "$configuration_file" ]; then
    source $configuration_file
else
    echo "Configuration file $configuration_file does not exist."
    echo ""
    echo "You should create one in the following format."
    echo ""
    cat configurations/example-configuration
    exit 1
fi

# import basic functions
source basic_functions.sh

# validate input
source validate_input.sh
