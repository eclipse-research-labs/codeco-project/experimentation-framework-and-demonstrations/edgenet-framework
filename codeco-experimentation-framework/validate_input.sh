#!/bin/sh

# Copyright (c) 2024 Athena RC.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0
#
# Contributors:
#      Lefteris Mamatas - author
#      George Koukis - contributor

# confirm that docker and jq are installed
check_if_docker_exists=$(is_docker_installed)
echo ""
if [[ $check_if_docker_exists == "true" ]]; then
   echo "Docker is installed, this is good."
else
   echo "You should install docker before proceeding."
   exit 1
fi

check_if_jq_exists=$(is_jq_installed)
if [[ $check_if_jq_exists == "true" ]]; then
   echo "jq tool is installed, this is good."
else
   echo "You should install jq before proceeding. You can try:"
   echo "sudo apt update"
   echo "sudo apt install -y jq"
   exit 1
fi

# Load valid options from JSON file
valid_options=$(jq '.' valid-configurations.json)

# Parse valid options
valid_kubernetes_types=$(echo "$valid_options" | jq -r '.kubernetes_type[]')
valid_networkfabrics=$(echo "$valid_options" | jq -r '.kubernetes_networkfabric[]')
valid_kubernetes_networkfabric_combinations=$(echo "$valid_options" | jq -r '.kubernetes_networkfabric_combinations')
valid_node_osimage=$(echo "$valid_options" | jq -r '.node_osimage[]')
valid_cloud_operators=$(echo "$valid_options" | jq -r '.cloud_operator[]')
valid_master_hosts=$(echo "$valid_options" | jq -r '.master_hosts[]')
valid_master_ips=$(echo "$valid_options" | jq -r '.master_ips[]')
valid_master_macs=$(echo "$valid_options" | jq -r '.master_macs[]')
valid_worker_hosts=$(echo "$valid_options" | jq -r '.worker_hosts[]')
valid_worker_ips=$(echo "$valid_options" | jq -r '.worker_ips[]')
valid_worker_macs=$(echo "$valid_options" | jq -r '.worker_macs[]')
valid_app_names=$(echo "$valid_options" | jq -r '.app_names[]')
valid_app_scopes=$(echo "$valid_options" | jq -r '.app_scopes[]')
valid_output_formats=$(echo "$valid_options" | jq -r '.output_formats[]')

# Function to parse JSON object into Bash associative array
parse_valid_combinations() {
    local json="$1"

    declare -A combinations
    while IFS="=" read -r key value; do
        combinations["$key"]="$value"
        echo "$key $value"
done < <(jq -r 'to_entries[] | "\(.key)=\(.value | join(" "))"' <<< "$json")

}

# Validate parameter function
validate_parameter() {
   local input="$1"
   local valid_options="$(echo "$2" | jq -R . | jq -s .)"
   local parameter_name="$3"

    # Validate input
    if ! echo "$valid_options" | grep -qw "$input"; then
        echo "Error: Invalid $parameter_name entry: $input. Valid options are: $(echo "$valid_options")"
        exit 1
    fi
}

# Validate array parameters function
validate_array_parameter() {
   local array_input="$1"
   local valid_options="$(echo "$2" | jq -R . | jq -s .)"
   local parameter_name="$3"

   # Check if the input is a valid JSON array
   validate_json_array_parameter "$array_input" "$parameter_name"

   # Check each element in the array
   local invalid_entries=0
   for entry in $(echo "$array_input" | jq -r '.[]'); do
       if ! echo "$valid_options" | grep -qwF "$entry"; then
	   echo "Error: Invalid $parameter_name entry: $entry. Valid options are: $(echo "$valid_options")" # | tr '\n' ', ' | sed 's/, $//')"
           invalid_entries=1
       fi
   done

   if [[ $invalid_entries -ne 0 ]]; then
       exit 1
   fi
}

# Function to check if a variable is a positive number
validate_numbered_parameter() {
    local input="$1"
    local parameter_name="$2"

    # Check if the variable is a positive number (integer or float)
    if [[ ! "$input" =~ ^[0-9]+([.][0-9]+)?$ ]]; then
       echo "Error: Invalid $parameter_name entry: $input. Valid values are positive numbers or zero."
       exit 1
    fi
}

# Function to validate if a variable is a valid IPv4 address
validate_ip_parameter() {
    local input="$1"
    local parameter_name="$2"

    # Regular expression to match a valid IPv4 address
    local regex='^([0-9]{1,3}\.){3}[0-9]{1,3}$'

    if [[ $input =~ $regex ]]; then
        # Split the IP address into its components and check each one
        IFS='.' read -r -a octets <<< "$input"
        for octet in "${octets[@]}"; do
            if ((octet < 0 || octet > 255)); then
                echo "Error: Invalid $parameter_name entry: $input. Valid value is an IPv4 address."
                exit 1
            fi
            # it is a valid IP address, do nothing
        done
    else
        echo "Error: Invalid $parameter_name entry: $input. Valid value is an IPv4 address."
        exit 1
    fi
}

# validate JSON string parameter
validate_json_string_parameter() {
    local json_input="$1"
    local parameter_name="$2"

    # Check if the input is a valid JSON string
    if ! echo "$json_input" | jq -e . >/dev/null 2>&1; then
        echo "Error: Invalid format for $parameter_name. It should be a valid JSON string."
        exit 1
    fi
}

# Validate JSON array parameter
validate_json_array_parameter() {
   local array_input="$1"
   local parameter_name="$2"

   # Check if the input is a valid JSON array
   if ! echo "$array_input" | jq -e . >/dev/null 2>&1; then
        echo "Error: Invalid format for $parameter_name. It should be a valid JSON array."
        exit 1
   fi
}

# Validate IP array parameters function
validate_ip_array_parameter() {
   local array_input="$1"
   local parameter_name="$2"

   # Check if the input is a valid JSON array
   validate_json_array_parameter "$array_input" "$parameter_name"

   # Check each element in the array
   local invalid_entries=0
   for entry in $(echo "$array_input" | jq -r '.[]'); do
       # check if this IP is valid
       validate_ip_parameter "$entry" "$parameter_name"
   done
}

validate_parameters() {
    # Extract valid combinations and store in associative array
    valid_combinations_array=$(parse_valid_combinations "$valid_kubernetes_networkfabric_combinations")

    # Validate kubernetes_type
    if ! echo "$valid_combinations_array" | grep -qw "$kubernetes_type"; then
        echo "Error: Invalid kubernetes_type. Valid options are: $(echo "$valid_kubernetes_types")"
        exit 1
    fi

    # Validate kubernetes_networkfabric
    local valid_network_fabrics=$(jq -r --arg type "$kubernetes_type" '.[$type] | join(" ")' <<< "$valid_kubernetes_networkfabric_combinations")
    if ! echo "$valid_network_fabrics" | grep -qw "$kubernetes_networkfabric"; then
	echo "Error: Invalid kubernetes_networkfabric for type $kubernetes_type. Valid options are: $(echo "$valid_network_fabrics")"
        exit 1
    fi

    # Basic validation for single-quoted JSON-like strings
    if [[ -n "$kubernetes_networkfabricparameters" && ! "$kubernetes_networkfabricparameters" =~ ^\{[[:space:]]*\'[a-zA-Z0-9_]+\'[[:space:]]*:[[:space:]]*\'[^\']*\'[[:space:]]*(,[[:space:]]*\'[a-zA-Z0-9_]+\'[[:space:]]*:[[:space:]]*\'[^\']*\')*[[:space:]]*\}$ ]]; then
        echo "Error: Invalid format for kubernetes_networkfabricparameters. It should adhere to the following example: kubernetes_networkfabricparameters=\"{'param1':'value1','param2':'value2'}\""
        exit 1
    fi

    # Validate node_osimage
    validate_parameter "$node_osimage" "$valid_node_osimage" "node_osimage"

    # Validate cloud_operator
    # If it is passed
    if [[ ! -z $cloud_operator ]]; then
       validate_parameter "$cloud_operator" "$valid_cloud_operators" "cloud_operator"
    fi

    # Validate cloud_ip
    # If it is passed
    if [[ ! -z $cloud_ip ]]; then
       validate_ip_parameter "$cloud_ip" "cloud_ip"
    fi

    # Validate master_cloud_ips
    if [[ ! -z $master_cloud_ips ]]; then
       validate_ip_array_parameter "$master_cloud_ips" "master_cloud_ips"
    fi

    # Validate worker_cloud_ips
    # If it is passed
    if [[ ! -z $worker_cloud_ips ]]; then
       validate_ip_array_parameter "$worker_cloud_ips" "worker_cloud_ips"
    fi

    # Validate master_cloud_operators (in the case they are many)
    validate_array_parameter "$master_cloud_operators" "$valid_cloud_operators" "master_cloud_operators"

    # Validate worker_cloud_operators (in the case they are many)
    validate_array_parameter "$worker_cloud_operators" "$valid_cloud_operators" "worker_cloud_operators"

    # Validate master_ips
    validate_array_parameter "$master_ips" "$valid_master_ips" "master_ips"

    # Validate master_macs
    validate_array_parameter "$master_macs" "$valid_master_macs" "master_macs"

    # Validate worker_hosts
    validate_array_parameter "$worker_hosts" "$valid_worker_hosts" "worker_hosts"

    # Validate worker_ips
    validate_array_parameter "$worker_ips" "$valid_worker_ips" "worker_ips"

    # Validate worker_macs
    validate_array_parameter "$worker_macs" "$valid_worker_macs" "worker_macs"

    # Validate app_names
    validate_array_parameter "$app_names" "$valid_app_names" "app_names"

    # Validate app_scopes
    validate_array_parameter "$app_scopes" "$valid_app_scopes" "app_scopes"

    # Validate inputs for experiment controller, in the case they are passed
    # ranged_variables: deployment configuration to range in each experimental run
    # replications_number: number of experiment replications for statistical accuracy purposes
    # configuration_file: configuration of infrastructure
    # app_names & app_scopes: benchmarking applications to run
    # benchmarking_command: benchmarking command to execute in each replicated experiment
    # replication_rest_time: warm-up period before each replication
    # run_rest_time: warm-up period before each deployment
    # output_format: format of experiment output, i.e., currently only json is supported
    # metrics: definition of metrics configuration in experiment
    # graphs: definition of graphs configuration in experiment

    # validate ranged_variables json array parameter
    if [[ ! -z $ranged_variables ]]; then
       validate_json_array_parameter "$ranged_variables" "ranged_variables"
    fi
   
    # validate replications_number parameter
    if [[ ! -z $replications_number ]]; then
       validate_numbered_parameter "$replications_number" "replications_number"
    fi

    # validate replication_rest_time parameter
    if [[ ! -z $replication_rest_time ]]; then
       validate_numbered_parameter "$replication_rest_time" "replication_rest_time"
    fi

    # validate run_rest_time parameter
    if [[ ! -z $run_rest_time ]]; then
       validate_numbered_parameter "$run_rest_time" "run_rest_time"
    fi

    # Validate output_format parameter
    # If it is passed
    if [[ ! -z $output_format ]]; then
       validate_parameter "$output_format" "$valid_output_formats" "output_format"
    fi

    # validate metrics json array parameter
    if [[ ! -z $metrics ]]; then
       validate_json_string_parameter "$metrics" "metrics"
    fi

    # validate graphs json array parameter
    if [[ ! -z $graphs ]]; then
       validate_json_array_parameter "$graphs" "graphs"
    fi

    echo "All input parameters are valid."
}

# Validate parameters
validate_parameters
