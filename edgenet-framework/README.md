# EdgeNet Framework | CODECO project
This repository is prepared by the [ATHENA-RC](https://www.athenarc.gr/en/home) team to demonstrate some EdgeNet demo and functionalities for the WP5 of the [CODECO project](https://he-codeco.eu/).

The examples of both the **demo** and **tutorials** folders were tested in the [EdgeNet testbed infrastructure](https://www.edge-net.org/). Please ensure that you **update the namespace** and the related information to match your own. In our demonstrations the namespace is the *athena-rc*.

On the contrary, the **installation** folder provides instructions on how to use the **EdgeNet software** to deploy CODECO-EdgeNet clusters that support fundamental EdgeNet features. 

## Demo Folder

Here, one can explore two [``.yaml`` files](https://kubernetes.io/docs/concepts/overview/working-with-objects/kubernetes-objects/), the [*``cdnserviceexample.yaml``*](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/blob/main/demo/cdnserviceexample.yaml) and [*``ping-me-example.yaml``*](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/blob/main/demo/ping-me-example.yaml). These files enable users to test fundamental EdgeNet functionalities, such as the [Selective Deployment](https://github.com/EdgeNet-project/edgenet/blob/main/docs/custom_resources.md#selective-deployment) which allows users deploy pods onto nodes based on their locations. In addition, instructions outlining the **prerequisites** for conducting experiments on EdgeNet, along with the requisite **commands** to execute the provided examples, are included. A demonstrative [**video**](https://github.com/gkoukis/MyTest/assets/127508084/942e05ad-2af0-484e-a80b-f984d562562d) featuring the deployment and outcome of these ``.yaml`` files is provided as well to enrich the understanding of the whole process.

## Tutorials Folder

In the [tutorials](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/tree/main/edgenet-framework/tutorials?ref_type=heads) folder we have included some ``.yaml`` files designed by EdgeNet to enable the utilization of a shared cluster environment. Within this folder, the ``.yaml`` files serve different cluster management purposes, for instance, to i) enable tenants to [assign roles](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/blob/main/edgenet-framework/tutorials/role_binding-ath-admin-george.yaml) per user, ii) allow users to [request roles](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/blob/main/edgenet-framework/tutorials/rolerequest-ath-george.yaml) within tenants or subnamespaces and iii) support the creation of subclusters, [slices](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/blob/main/edgenet-framework/tutorials/sliceclaim-ath.yaml) and [subnamespaces](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/blob/main/edgenet-framework/tutorials/subnamespace-Workspace-ath.yaml). For more detailed information, please refer to the EdgeNet repository [here](https://github.com/EdgeNet-project/edgenet/tree/main/docs/tutorials).

## Installation Folder
In the [installation](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/experimentation-framework/-/tree/main/edgenet-framework/installation) folder you can find the documentation for **Deploying EdgeNet features/CRDs within a local Kubernetes cluster environment**. We have forked the code from the official EdgeNet repository and contributed with minor bug fixes and improvements to enhance the overall functionality of the infrastructure. In particular, we managed to utilize the **Multiprovider** (i.e. contribute a node into an EdgeNet cluster) and **Multitenancy** (enabling the utilization of a shared cluster such as tenant and role requests, slice and subnamespaces) features.

## Upcoming Tasks
~~**[TODO]:** We are currently in the process of testing the deployment of EdgeNet functionalities (**Multiprovider, Multitenancy**) within our Kubernetes cluster environment as claimed in [here](https://github.com/EdgeNet-project/edgenet/blob/main/docs/tutorials/deploy_edgenet_to_kube.md).~~

**[TODO]:** Ensure the functionality of the provided EdgeNet features, due to the [EdgeNet archived repository](https://github.com/EdgeNet-project/edgenet-legacy-2024).

**[TODO]:** We are currently in the process of testing the deployment of EdgeNet functionalities (**Selective Deployment**) within our Kubernetes cluster environment as claimed in [here](https://github.com/EdgeNet-project/edgenet/blob/main/docs/tutorials/deploy_edgenet_to_kube.md).

**[TODO]:** Aligning with EdgeNet's instructions for [contribution guidelines](https://github.com/EdgeNet-project/edgenet/blob/main/docs/guides/contribution_guides.md).

For a more in-depth exploration of **EdgeNet** one can visit the [EdgeNet-Testbed site](https://www.edge-net.org/pages/running-experiments.html) and its corresponding [Github repository](https://github.com/EdgeNet-project/edgenet).
