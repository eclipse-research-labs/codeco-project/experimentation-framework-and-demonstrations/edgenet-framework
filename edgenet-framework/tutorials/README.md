# EdgeNet cluster utilization files | CODECO project

EdgeNet custom resources to enable the utilization of a shared cluster. 

**rolerequest-ath-george.yaml**: request a role in a tenant or a subnamespace to receive permissions corresponding to the requested role in the associated namespace.

**role_binding-ath-admin-george.yaml**: grant users access to the resources under your tenant's control as the tenant owner or admin.

**sliceclaim-ath.yaml**: Creates a node-level slice on EdgeNet, that allows slicing among nodes by defining a selector. It forms a subcluster, when complete isolation and flexible configuration is required with tenant owner one of the tenants or subnamespace admins.

**subnamespace-Subtenant-ath-george.yaml** and **subnamespace-Workspace-ath.yaml**: Create subnamespaces that share the assigned tenant's resource quota - i.e. generates a child namespace that inherits role-based access control and network policy settings from its parent.

For a more in-depth description of the functionalities visit the EdgeNet repository [here](https://github.com/EdgeNet-project/edgenet/blob/main/docs/README.md#multitenancy) and [here](https://github.com/EdgeNet-project/edgenet/tree/main/docs/tutorials).